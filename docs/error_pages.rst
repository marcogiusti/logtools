Exclude the static files from the logs and print the statistics.

5% of the requests returned in more than 7.81s and 1% of the requests
returned in more than 34.89s.

Error rate is 1.36%. The webserver is actually lying because in many
cases it returns an error page with the status code 200.

::

   $ logfilter --exclude --static-files www1.log | \
      loganon | \
      logstats

::

   General statistics
    num requests:  163123
    first request: 22:00:00 15/10/2019
    last request:  11:25:31 16/10/2019
    min duration:  0 ms (V9IQ2msNUTNqgtdzVPxrB2lLb-EiwMIyt4rTiR-ODC0)
    max duration:  321.87 s (OnfFDUNmcuOlWovRlUIqn_e-qrnAanQJQml2rbTGfZo)
    mean:          1533.40 ms
    std deviation: 6.89 s
    variance:      47493.00 s
    median:        78.00 ms
    mode:          1.00 ms
    percentiles:   25th: 15 ms
                   50th: 78 ms
                   75th: 239 ms
                   90th: 1222 ms
                   95th: 7.81 s
                   99th: 34.89 s
    errors:        2226 (1.36%)

   Most common pages
    VSqW6uZozfXC1AzBvJinRTh6SqSofs4jF2QQM9pjzNM........................... 27271
    eARO8ccZ-yPMVBFYlfAZAd1kmP07HtlGqkdD6DiYg9o........................... 15049
    alcHqZ8YH6XVyixv-JWTWey44DUh6VmtCyvo8xvseYA........................... 12285
    q42-hdIs72HCD7KA3TwkYWsaxexM6_ioj9tYTsqYpNY........................... 11951
    KK3cGO3assNhUdC0I78PYLO-4N9iCjYA2ERW-idOY9M........................... 11183

   Most expensive pages (cumulated time)
    N0kMQ3LrYforyvFyyjJCSVfYFAHpSXYU-GmbVygpKzc........................... 43280519
    T8lQgGZnHOCDhobC7NzI7J1pxh1QlCw9I9S7Zm6-XrQ........................... 23827146
    eARO8ccZ-yPMVBFYlfAZAd1kmP07HtlGqkdD6DiYg9o........................... 22461307
    VSqW6uZozfXC1AzBvJinRTh6SqSofs4jF2QQM9pjzNM........................... 19640610
    q42-hdIs72HCD7KA3TwkYWsaxexM6_ioj9tYTsqYpNY........................... 16813168

   Most error pages
    N0kMQ3LrYforyvFyyjJCSVfYFAHpSXYU-GmbVygpKzc........................... 351
    VSqW6uZozfXC1AzBvJinRTh6SqSofs4jF2QQM9pjzNM........................... 317
    q42-hdIs72HCD7KA3TwkYWsaxexM6_ioj9tYTsqYpNY........................... 303
    eARO8ccZ-yPMVBFYlfAZAd1kmP07HtlGqkdD6DiYg9o........................... 294
    T8lQgGZnHOCDhobC7NzI7J1pxh1QlCw9I9S7Zm6-XrQ........................... 264


From the chart we see that errors happens before 10am CEST, (8am UTC).

::

   $ logfilter --exclude --static-files www1.log | \
      logsample --size 5000 | \
      logplot --group status_code --save-plot error_pages.png

.. image:: error_pages.png

Closer view of what happes between 8am and 10:30am.

::

   $ logfilter --from 2019-10-16T08 --thru 2019-10-16T10:30 www1.log | \
      logfilter --exclude --static-files | \
      logsample --size 5000 | \
      logplot --logscale --group status_code --save-plot error_pages_8_00_10_30.png

.. image:: error_pages_8_00_10_30.png

.. vim: ft=rst tw=72
