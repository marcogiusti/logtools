# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from dataclasses import fields
from datetime import datetime, date, time, timezone
from io import StringIO
from typing import Optional
import unittest

from logtools._w3clogging import (
    FieldIdentifier,
    load_directives,
    make_w3c_ext_log_event_class,
    make_w3c_ext_log_event_parser,
)


def list_directives(file_content, **kwargs):
    fp = StringIO(file_content)
    directives = list(load_directives(fp, **kwargs))
    return directives


class TestExtendedLogFileFormat(unittest.TestCase):
    def test_load_directives(self):
        content = """\
# Version: 1.0
# Software: logtools
# Start-Date: 2021-09-21 13:46:04
# End-Date: 2021-09-21 14:00:00
# Date: 2021-09-21 13:51:10
# Remark: description
        """
        expected_directives = [
            ("Version", "1.0"),
            ("Software", "logtools"),
            ("Start-Date", datetime(2021, 9, 21, 13, 46, 4, tzinfo=timezone.utc)),
            ("End-Date", datetime(2021, 9, 21, 14, tzinfo=timezone.utc)),
            ("Date", datetime(2021, 9, 21, 13, 51, 10, tzinfo=timezone.utc)),
            ("Remark", "description"),
        ]
        self.assertEqual(list_directives(content), expected_directives[:4])
        self.assertEqual(
            list_directives(content, max_num_lines=5), expected_directives[:5]
        )
        self.assertEqual(
            list_directives(content, max_num_lines="*"), expected_directives
        )

    def test_load_directives_err(self):
        content = """\
# Version: 1.0
# Invalid-directive
# Start-Date: 2021-09-21 13:46:04
        """
        expected_directives = [
            ("Version", "1.0"),
            ("Start-Date", datetime(2021, 9, 21, 13, 46, 4, tzinfo=timezone.utc)),
        ]
        fp = StringIO(content)
        directives = list(load_directives(fp))
        self.assertEqual(directives, expected_directives)
        fp = StringIO(content)
        # preserve the fp position
        self.assertEqual(fp.tell(), 0)
        with self.assertRaises(ValueError):
            list(load_directives(fp, ignore_errors=False))
        self.assertEqual(fp.tell(), 15)
        # First invalid line
        self.assertEqual(next(fp), "# Invalid-directive\n")

    def test_load_fields(self):
        s = "# Fields: date time time-taken bytes cached"
        expected = [
            (
                "Fields",
                [
                    FieldIdentifier.simple("date"),
                    FieldIdentifier.simple("time"),
                    FieldIdentifier.simple("time-taken"),
                    FieldIdentifier.simple("bytes"),
                    FieldIdentifier.simple("cached"),
                ],
            ),
        ]
        self.assertEqual(list_directives(s), expected)
        s = "# Fields: c-method s-uri"
        expected = [
            (
                "Fields",
                [
                    FieldIdentifier.with_prefix("c", "method"),
                    FieldIdentifier.with_prefix("s", "uri"),
                ],
            ),
        ]
        self.assertEqual(list_directives(s), expected)
        s = "# Fields: sc(Referer)"
        expected = [
            ("Fields", [FieldIdentifier.header("sc", "Referer")]),
        ]
        self.assertEqual(list_directives(s), expected)

    def test_make_w3c_ext_log_event_class(self):
        def assertFieldsEqual(fields_identifiers, expected_fields):
            CustomW3CExtLogEvent = make_w3c_ext_log_event_class(fields_identifiers)
            calculated_fields = [(f.name, f.type) for f in fields(CustomW3CExtLogEvent)]
            self.assertEqual(expected_fields, calculated_fields)

        assertFieldsEqual(
            [FieldIdentifier.simple("date")],
            [("date", Optional[date]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.simple("time")],
            [("time", Optional[time]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.simple("date"), FieldIdentifier.simple("time")],
            [
                ("date", Optional[date]),
                ("time", Optional[time]),
                ("timestamp", Optional[datetime]),
                ("_line", Optional[str]),
            ],
        )
        assertFieldsEqual(
            [FieldIdentifier.simple("port")],
            [("port", Optional[int]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.with_prefix("sc", "status")],
            [("status_code", Optional[int]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.simple("time-taken")],
            [("duration", Optional[int]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.with_prefix("s", "ip")],
            [("server_host", Optional[str]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.with_prefix("c", "ip")],
            [("remote_host", Optional[str]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.with_prefix("cs", "method")],
            [("method", Optional[str]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.with_prefix("cs", "uri-stem")],
            [("uri", Optional[str]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.with_prefix("cs", "uri-query")],
            [("query", Optional[str]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.with_prefix("cs", "username")],
            [("userid", Optional[str]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.with_prefix("cs", "user-agent")],
            [("user_agent", Optional[str]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.simple("bytes")],
            [("size", Optional[int]), ("_line", Optional[str])],
        )
        assertFieldsEqual(
            [FieldIdentifier.header("sc", "Referer")],
            [("referer", Optional[str]), ("_line", Optional[str])],
        )

    def test_make_w3c_ext_log_event_class_str(self):
        def assertEventStringEqual(fields_identifiers, expected, **kwargs):
            LogEvent = make_w3c_ext_log_event_class(fields_identifiers)
            event = LogEvent(**kwargs)
            self.assertEqual(str(event), expected)

        assertEventStringEqual(
            [FieldIdentifier.simple("date")],
            "2021-09-26",
            date=date(2021, 9, 26),
            _line="2021-09-26",
        )
        assertEventStringEqual(
            [FieldIdentifier.simple("time")],
            "10:13:09",
            time=time(10, 13, 9),
            _line="10:13:09",
        )
        assertEventStringEqual(
            [FieldIdentifier.simple("date"), FieldIdentifier.simple("time")],
            "2021-09-26 10:13:09",
            date=date(2021, 9, 26),
            time=time(10, 13, 9),
            timestamp=datetime(2021, 9, 26, 10, 13, 9),
            _line="2021-09-26 10:13:09",
        )
        assertEventStringEqual(
            [FieldIdentifier.simple("port")], "80", port=80, _line="80"
        )
        assertEventStringEqual(
            [FieldIdentifier.with_prefix("sc", "status")],
            "200",
            status_code=200,
            _line="200",
        )
        assertEventStringEqual(
            [FieldIdentifier.simple("time-taken")],
            "123",
            duration=123,
            _line="123",
        )
        assertEventStringEqual(
            [FieldIdentifier.with_prefix("s", "ip")],
            "10.0.1.2",
            server_host="10.0.1.2",
            _line="10.0.1.2",
        )
        assertEventStringEqual(
            [FieldIdentifier.with_prefix("c", "ip")],
            "1.2.3.4",
            remote_host="1.2.3.4",
            _line="1.2.3.4",
        )
        assertEventStringEqual(
            [FieldIdentifier.with_prefix("cs", "method")],
            "GET",
            method="GET",
            _line="GET",
        )
        assertEventStringEqual(
            [FieldIdentifier.with_prefix("cs", "uri-stem")],
            "/robots.txt",
            uri="/robots.txt",
            _line="/robots.txt",
        )
        assertEventStringEqual(
            [FieldIdentifier.with_prefix("cs", "uri-query")],
            "name=value",
            query="name=value",
            _line="name=value",
        )
        assertEventStringEqual(
            [FieldIdentifier.with_prefix("cs", "username")],
            "alice",
            userid="alice",
            _line="alice",
        )
        assertEventStringEqual(
            [FieldIdentifier.with_prefix("cs", "user-agent")],
            "Firefox(92.0)",
            user_agent="Firefox(92.0)",
            _line="Firefox(92.0)",
        )
        assertEventStringEqual(
            [FieldIdentifier.simple("bytes")], "54", size=54, _line="54"
        )
        assertEventStringEqual(
            [FieldIdentifier.header("sc", "Referer")],
            "https://developer.mozilla.org/en-US/docs/Web/JavaScript",
            referer="https://developer.mozilla.org/en-US/docs/Web/JavaScript",
            _line="https://developer.mozilla.org/en-US/docs/Web/JavaScript",
        )
        # Generic identifier with prefix
        assertEventStringEqual(
            [FieldIdentifier.with_prefix("sc", "win32-status")],
            "0",
            sc_win32_status="0",
            _line="0",
        )
        # Generic header identifier
        assertEventStringEqual(
            [FieldIdentifier.header("cs", "Accept-Encoding")],
            "gzip,+deflate,+br",
            cs_accept_encoding="gzip,+deflate,+br",
            _line="gzip,+deflate,+br",
        )
        # Missing field
        assertEventStringEqual(
            [FieldIdentifier.with_prefix("cs", "username")],
            "-",
            userid=None,
            _line="-",
        )

    def test_make_w3c_ext_log_event_parser(self):
        def assertEventEqual(field_identifiers, line, **kwargs):
            parser = make_w3c_ext_log_event_parser(field_identifiers)
            expected_event = parser._LogEvent(**kwargs)
            self.assertEqual(parser.parse_line(line), expected_event)

        assertEventEqual(
            [FieldIdentifier.simple("date")],
            "2021-09-26",
            date=date(2021, 9, 26),
            _line="2021-09-26",
        )

        assertEventEqual(
            [FieldIdentifier.simple("time")],
            "10:13:09",
            time=time(10, 13, 9, tzinfo=timezone.utc),
            _line="10:13:09",
        )
        assertEventEqual(
            [FieldIdentifier.simple("date"), FieldIdentifier.simple("time")],
            "2021-09-26 10:13:09",
            date=date(2021, 9, 26),
            time=time(10, 13, 9, tzinfo=timezone.utc),
            timestamp=datetime(2021, 9, 26, 10, 13, 9, tzinfo=timezone.utc),
            _line="2021-09-26 10:13:09",
        )
        assertEventEqual([FieldIdentifier.simple("port")], "80", port=80, _line="80")
        assertEventEqual(
            [FieldIdentifier.with_prefix("sc", "status")],
            "200",
            status_code=200,
            _line="200",
        )
        assertEventEqual(
            [FieldIdentifier.simple("time-taken")], "123", duration=123, _line="123"
        )
        assertEventEqual(
            [FieldIdentifier.with_prefix("s", "ip")],
            "10.0.1.2",
            server_host="10.0.1.2",
            _line="10.0.1.2",
        )
        assertEventEqual(
            [FieldIdentifier.with_prefix("c", "ip")],
            "1.2.3.4",
            remote_host="1.2.3.4",
            _line="1.2.3.4",
        )
        assertEventEqual(
            [FieldIdentifier.with_prefix("cs", "method")],
            "GET",
            method="GET",
            _line="GET",
        )
        assertEventEqual(
            [FieldIdentifier.with_prefix("cs", "uri-stem")],
            "/robots.txt",
            uri="/robots.txt",
            _line="/robots.txt",
        )
        assertEventEqual(
            [FieldIdentifier.with_prefix("cs", "uri-query")],
            "name=value",
            query="name=value",
            _line="name=value",
        )
        assertEventEqual(
            [FieldIdentifier.with_prefix("cs", "username")],
            "alice",
            userid="alice",
            _line="alice",
        )
        assertEventEqual(
            [FieldIdentifier.with_prefix("cs", "user-agent")],
            "Firefox(92.0)",
            user_agent="Firefox(92.0)",
            _line="Firefox(92.0)",
        )
        assertEventEqual([FieldIdentifier.simple("bytes")], "54", size=54, _line="54")
        assertEventEqual(
            [FieldIdentifier.header("sc", "Referer")],
            "https://developer.mozilla.org/en-US/docs/Web/JavaScript",
            referer="https://developer.mozilla.org/en-US/docs/Web/JavaScript",
            _line="https://developer.mozilla.org/en-US/docs/Web/JavaScript",
        )
        # Generic identifier with prefix
        assertEventEqual(
            [FieldIdentifier.with_prefix("sc", "win32-status")],
            "0",
            sc_win32_status="0",
            _line="0",
        )
        # Generic header identifier
        assertEventEqual(
            [FieldIdentifier.header("cs", "Accept-Encoding")],
            "gzip,+deflate,+br",
            cs_accept_encoding="gzip,+deflate,+br",
            _line="gzip,+deflate,+br",
        )
        # Missing field
        assertEventEqual(
            [FieldIdentifier.with_prefix("cs", "username")],
            "-",
            userid=None,
            _line="-",
        )
