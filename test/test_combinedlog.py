# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from datetime import datetime, timedelta, timezone
from io import StringIO
import unittest

from logtools._combinedlog import CombinedLogEvent, CombinedLogParser
from logtools._testing import dummy_sample


LOG_LINE = '127.0.0.1 - frank [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326 "http://www.example.com/start.html" "Mozilla/4.08 [en] (Win98; I ;Nav)"'
LOG_LINE_WITH_SEARCH_PARAMS = '127.0.0.1 - frank [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif?password=qwerty HTTP/1.0" 200 2326 "http://www.example.com/start.html" "Mozilla/4.08 [en] (Win98; I ;Nav)"'
LOG_LINE_INVALID_MONTH = '127.0.0.1 - frank [10/Ott/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326 "http://www.example.com/start.html" "Mozilla/4.08 [en] (Win98; I ;Nav)"'


class TestParser(unittest.TestCase):
    def test_parse_line(self):
        parser = CombinedLogParser()
        event = parser.parse_line(LOG_LINE)
        self.assertEqual(event.remote_host, "127.0.0.1")
        self.assertEqual(event.identity, "-")
        self.assertEqual(event.userid, "frank")
        self.assertEqual(
            event.timestamp,
            datetime(2000, 10, 10, 13, 55, 36, tzinfo=timezone(-timedelta(hours=7))),
        )
        self.assertEqual(event.method, "GET")
        self.assertEqual(event.uri, "/apache_pb.gif")
        self.assertEqual(event.query, None)
        self.assertEqual(event.protocol, "HTTP/1.0")
        self.assertEqual(event.status_code, 200)
        self.assertEqual(event.size, 2326)
        self.assertEqual(event.referer, "http://www.example.com/start.html")
        self.assertEqual(event.user_agent, "Mozilla/4.08 [en] (Win98; I ;Nav)")

    def test_parse_logs(self):
        parser = CombinedLogParser()
        expected_events = [parser.parse_line(LOG_LINE)]
        fp = StringIO(LOG_LINE)
        events = list(parser.parse_logs([fp]))
        self.assertEqual(events, expected_events)
        fp.seek(0)
        events = list(parser.parse_logs([fp], sample=dummy_sample))
        self.assertEqual(events, expected_events)

    def test_has_field(self):
        parser = CombinedLogParser()
        self.assertTrue(parser.has_field("remote_host"))
        self.assertFalse(parser.has_field("c-ip"))

    def test_invalid_lines(self):
        parser = CombinedLogParser()
        self.assertRaises(ValueError, parser.parse_line, "sadfasdf")
        self.assertRaises(ValueError, parser.parse_line, LOG_LINE_INVALID_MONTH)

    def test_serialize(self):
        parser = CombinedLogParser()
        event = parser.parse_line(LOG_LINE)
        fp = StringIO()
        parser.serialize_logs([event], fp)
        self.assertEqual(fp.getvalue(), f"{event}\n")
        event2 = parser.parse_line(LOG_LINE_WITH_SEARCH_PARAMS)
        # replace to invalidate the cache
        event2 = event2.replace()
        fp2 = StringIO()
        parser.serialize_logs([event2], fp2)
        self.assertEqual(fp2.getvalue(), f"{event2}\n")


class TestEvent(unittest.TestCase):
    def test_methods(self):
        event = CombinedLogEvent(
            remote_host="127.0.0.1",
            identity="-",
            userid="frank",
            timestamp=datetime(
                2000, 10, 10, 13, 55, 36, tzinfo=timezone(-timedelta(hours=7))
            ),
            method="GET",
            uri="/apache_pb.gif",
            query=None,
            protocol="HTTP/1.0",
            status_code=200,
            size=2326,
            referer="http://www.example.com/start.html",
            user_agent="Mozilla/4.08 [en] (Win98; I ;Nav)",
            _line=LOG_LINE,
        )
        self.assertEqual(event._line, LOG_LINE)
        self.assertEqual(event.method, "GET")
        self.assertEqual(str(event), LOG_LINE)
        event = event.replace(method="POST")
        self.assertEqual(event.method, "POST")
        self.assertIsNone(event._line)
        self.assertEqual(str(event), LOG_LINE.replace("GET", "POST"))
