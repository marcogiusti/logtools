# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from datetime import timedelta, timezone
from argparse import ArgumentTypeError
import unittest

from logtools._combinedlog import CombinedLogParser
from logtools._scripts import logparser_t, timezone_t
from logtools._w3cextlog import W3CExtLogParser


class TestUtils(unittest.TestCase):
    def test_log_parser(self):
        self.assertIsInstance(logparser_t("combinedlog"), CombinedLogParser)
        self.assertIsInstance(logparser_t("w3cextlog"), W3CExtLogParser)
        # not a callable
        self.assertRaises(ArgumentTypeError, logparser_t, "logtools.__author__")

    def test_timezone_t(self):
        self.assertEqual(timezone_t("0"), timezone(timedelta(0)))
        self.assertEqual(timezone_t("+09"), timezone(timedelta(hours=9)))
        self.assertEqual(timezone_t("+09"), timezone(timedelta(hours=9)))
        self.assertEqual(timezone_t("-09"), timezone(timedelta(hours=-9)))
        self.assertEqual(timezone_t("-9"), timezone(timedelta(hours=-9)))
