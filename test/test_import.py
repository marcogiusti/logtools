import unittest

from logtools._import import InvalidSpecError, resolve
from logtools._parse_params import parse


class TestParseArgs(unittest.TestCase):
    def test_parse_args(self):
        self.assertEqual(
            parse("tcp:host=www.example.com:port=80"),
            (["tcp"], {"host": "www.example.com", "port": "80"}),
        )
        self.assertEqual(
            parse("tcp:www.example.com:80"),
            (["tcp", "www.example.com", "80"], {}),
        )
        self.assertEqual(
            parse("tcp:host=www.example.com:80"),
            (["tcp", "80"], {"host": "www.example.com"}),
        )
        self.assertEqual(
            parse("tcp:www.example.com:port=80"),
            (["tcp", "www.example.com"], {"port": "80"}),
        )
        self.assertEqual(parse(""), ([""], {}))


class TestResolveSpec(unittest.TestCase):
    def test_resolve_spec(self):
        self.assertIs(
            resolve("logtools._import.InvalidSpecError"), InvalidSpecError
        )
        self.assertRaises(ImportError, resolve, "logtools.unexisting_module")
        self.assertRaises(ImportError, resolve, "logtools._import.unexisting_obj")
        self.assertRaises(InvalidSpecError, resolve, "")
        self.assertRaises(InvalidSpecError, resolve, ".foo")
        self.assertRaises(InvalidSpecError, resolve, "bar.")
