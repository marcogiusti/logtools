# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from datetime import datetime, timedelta, timezone
import unittest

from logtools._stats import (
    Counter,
    Counters,
    Stats,
    format_counter,
    format_duration,
    format_stats,
    generate_stats,
    generate_stats_text,
    generate_stats_csv,
)
from logtools._testing import CEST, LogEvent


class TestStats(unittest.TestCase):
    def test_format_duration(self):
        self.assertEqual(format_duration(1999.9), "1999.90 ms")
        self.assertEqual(format_duration(1999), "1999 ms")
        self.assertEqual(format_duration(2000), "2.00 s")

    def test_format_counter(self):
        counter = Counter({"/a": 1, "/b": 3, "/c": 2})
        expected_lines = (
            " /b.................................................................... 3\n"
            " /c.................................................................... 2\n"
            " /a.................................................................... 1"
        )
        self.assertEqual(format_counter(counter), expected_lines)
        expected_lines = (
            " /b.................................................................... 3\n"
            " /c.................................................................... 2"
        )
        self.assertEqual(format_counter(counter, 2), expected_lines)

    def test_generate_stats(self):
        stats = generate_stats([LogEvent()])
        expected_stats = Stats(
            num_req=1,
            mean=196,
            median=196,
            mode=196,
            variance=None,
            stdev=None,
            percentiles=[None] * 100,
            errors=0,
            errors_percentage=0.0,
            first_request=datetime(2000, 10, 9, 13, 55, 35, tzinfo=CEST),
            last_request=datetime(2000, 10, 9, 13, 55, 35, tzinfo=CEST),
            min_duration=196,
            min_duration_page="/apache_pb.gif",
            max_duration=196,
            max_duration_page="/apache_pb.gif",
            counters=Counters(
                Counter({"/apache_pb.gif": 1}),
                Counter({"/apache_pb.gif": 196}),
                Counter(),
            ),
        )
        self.assertEqual(stats, expected_stats)

    def test_template(self):
        stats = generate_stats([LogEvent()])
        expected = """
General statistics
 num requests:  1
 first request: 13:55:35 09/10/2000
 last request:  13:55:35 09/10/2000
 min duration:  196 ms (/apache_pb.gif)
 max duration:  196 ms (/apache_pb.gif)
 mean:          196 ms
 std deviation: -
 variance:      -
 median:        196 ms
 mode:          196 ms
 percentiles:   25th: -
                50th: -
                75th: -
                90th: -
                95th: -
                99th: -
 errors:        0 (0.00%)

Most common pages
 /apache_pb.gif........................................................ 1

Most expensive pages (cumulated time)
 /apache_pb.gif........................................................ 196 ms

Most error pages

"""

        output = format_stats(stats, max_num_entries=2)
        self.assertEqual(output, expected)
        self.assertEqual(generate_stats_text([LogEvent()], 2), expected)


class TestCSVStats(unittest.TestCase):
    def test_generate_stats_csv(self):
        logevents = [LogEvent()]
        stats = list(generate_stats_csv(logevents))
        header = [
            "num_req",
            "mean",
            "median",
            "mode",
            "variance",
            "stdev",
            "percentiles",
            "errors",
            "errors_percentage",
            "first_request",
            "last_request",
            "min_duration",
            "min_duration_page",
            "max_duration",
            "max_duration_page",
        ]
        first_line_stats = (
            1,
            196,
            196,
            196,
            None,
            None,
            [None] * 100,
            0,
            0.0,
            datetime(2000, 10, 9, 13, 55, 35, tzinfo=CEST),
            datetime(2000, 10, 9, 13, 55, 35, tzinfo=CEST),
            196,
            "/apache_pb.gif",
            196,
            "/apache_pb.gif",
        )
        self.assertEqual(stats, [header, first_line_stats])
