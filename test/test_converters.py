# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from datetime import date, datetime, timedelta, timezone
from ipaddress import IPv4Address, IPv4Network
import sys
import unittest

from logtools._converters import ipaddr, isodate, isodatetime, positive_int


def is_naive(dt):
    return dt.tzinfo is None or dt.tzinfo.utcoffset(dt) is None


class TestConverters(unittest.TestCase):
    def test_positive_int(self):
        self.assertEqual(positive_int("2"), 2)
        self.assertRaises(ValueError, positive_int, "0")
        self.assertRaises(ValueError, positive_int, "-3")

    def test_ipaddr(self):
        self.assertEqual(ipaddr("1.2.3.4"), IPv4Address("1.2.3.4"))
        self.assertEqual(ipaddr("192.164.0.0/16"), IPv4Network("192.164.0.0/16"))
        self.assertRaises(ValueError, ipaddr, "2001:db8::8a2e:370:7334")

    def test_isodate(self):
        self.assertEqual(isodate("2021-09-04"), date(2021, 9, 4))
        self.assertRaises(ValueError, isodate, "2021.09.04")

    def test_isodatetime(self):
        self.assertEqual(
            isodatetime("2021-09-04T06:25:32+00:00"),
            datetime(2021, 9, 4, 6, 25, 32, tzinfo=timezone.utc),
        )
        self.assertEqual(
            isodatetime("2021-09-04T08:25:56+02:00"),
            datetime(2021, 9, 4, 8, 25, 56, tzinfo=timezone(timedelta(hours=2))),
        )
        self.assertEqual(
            isodatetime("2021-09-04T08:25:56+02:00"),
            datetime(2021, 9, 4, 6, 25, 56, tzinfo=timezone.utc),
        )
        if sys.version_info < (3, 11):
            self.assertRaises(
                ValueError, isodatetime, "2021-09-04T08:26:09,928128818+02:00"
            )

    def test_isodatetime_naive(self):
        dt = isodatetime("2021-09-04T06:25:32")
        self.assertIsInstance(dt, datetime)
        self.assertFalse(is_naive(dt))
