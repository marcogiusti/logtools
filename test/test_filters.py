# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from datetime import datetime, date
from ipaddress import IPv4Network
import unittest

from logtools._filters import (
    file_extension_eq,
    is_static_file,
    method_eq,
    remote_host_eq,
    slower_than,
    status_code_eq,
    timestamp_gte,
    timestamp_lte,
    uri_eq,
)
from logtools._testing import CEST, LogEvent


class TestStatusCodeFilters(unittest.TestCase):
    def assert_status_code_equal(self, status_code_pattern, event):
        test = status_code_eq(status_code_pattern)
        self.assertTrue(test(event))

    def assert_status_code_not_equal(self, status_code_pattern, event):
        test = status_code_eq(status_code_pattern)
        self.assertFalse(test(event))

    def test_status_code_xxx(self):
        self.assert_status_code_not_equal("1xx", LogEvent(status_code=200))
        self.assert_status_code_equal("2xx", LogEvent(status_code=200))
        self.assert_status_code_equal("2xx", LogEvent(status_code=201))
        self.assert_status_code_not_equal("2xx", LogEvent(status_code=404))
        self.assert_status_code_equal("3xx", LogEvent(status_code=302))
        self.assert_status_code_equal("4xx", LogEvent(status_code=404))
        self.assert_status_code_equal("5xx", LogEvent(status_code=500))
        self.assert_status_code_equal(404, LogEvent(status_code=404))
        self.assert_status_code_not_equal(404, LogEvent(status_code=418))
        self.assertRaises(ValueError, status_code_eq, "four-oh-four")
        self.assertRaises(ValueError, status_code_eq, 999)


class TestTimestampFilters(unittest.TestCase):
    def assert_timestamp_gte(self, timestamp, event):
        test = timestamp_gte(timestamp)
        self.assertTrue(test(event))

    def assert_timestamp_not_gte(self, timestamp, event):
        test = timestamp_gte(timestamp)
        self.assertFalse(test(event))

    def assert_timestamp_lte(self, timestamp, event):
        test = timestamp_lte(timestamp)
        self.assertTrue(test(event))

    def test_timestamp_filters(self):
        # gte
        reference_datetime = datetime(2000, 10, 10, 13, 55, 36, tzinfo=CEST)
        event = LogEvent(timestamp=datetime(2000, 10, 10, 13, 55, 36, tzinfo=CEST))
        self.assert_timestamp_gte(reference_datetime, event)
        event2 = LogEvent(timestamp=datetime(2000, 10, 10, 13, 55, 35, tzinfo=CEST))
        self.assert_timestamp_not_gte(reference_datetime, event2)
        reference_date = date(2000, 10, 10)
        self.assert_timestamp_gte(reference_date, event)
        self.assert_timestamp_gte(reference_date, event2)
        # lte
        self.assert_timestamp_lte(reference_datetime, event)
        self.assert_timestamp_lte(reference_datetime, event2)
        self.assert_timestamp_lte(reference_date, event)
        self.assert_timestamp_lte(reference_date, event2)


class TestFilters(unittest.TestCase):
    def test_slower_than(self):
        test = slower_than(200)
        self.assertFalse(test(LogEvent(duration=196)))
        self.assertTrue(test(LogEvent(duration=232)))

    def test_uri_filter(self):
        test = uri_eq("/apache_pb.gif")
        self.assertTrue(test(LogEvent(uri="/apache_pb.gif")))
        self.assertFalse(test(LogEvent(uri="/apache_pb.png")))

    def test_method_filter(self):
        test = method_eq("GET")
        self.assertTrue(test(LogEvent(method="GET")))
        self.assertFalse(test(LogEvent(method="POST")))

    def test_remote_host_filter(self):
        test = remote_host_eq("218.195.205.6")
        self.assertTrue(test(LogEvent(remote_host="218.195.205.6")))
        self.assertFalse(test(LogEvent(remote_host="1.2.3.4")))
        test = remote_host_eq(IPv4Network("218.195.205.6"))
        self.assertTrue(test(LogEvent(remote_host="218.195.205.6")))
        test = remote_host_eq(IPv4Network("218.195.205.0/24"))
        self.assertTrue(test(LogEvent(remote_host="218.195.205.6")))
        self.assertFalse(test(LogEvent(remote_host="218.195.204.11")))

    def test_file_extension_eq(self):
        test = file_extension_eq(["html"])
        self.assertTrue(test(LogEvent(uri="/index.html")))
        self.assertFalse(test(LogEvent(uri="/index.htm")))
        self.assertFalse(test(LogEvent(uri="/")))
        self.assertFalse(test(LogEvent(uri="/index.asp")))
        self.assertFalse(test(LogEvent(uri="/index_html")))

    def test_is_static_file(self):
        test = is_static_file()
        self.assertTrue(test(LogEvent(uri="/dot.gif")))
        self.assertFalse(test(LogEvent(uri="/")))
        self.assertFalse(test(LogEvent(uri="/index.asp")))
        self.assertFalse(test(LogEvent(uri="/dot_gif")))
