# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

import unittest
from dataclasses import dataclass
from datetime import datetime, timedelta, timezone
from ipaddress import IPv4Address

from logtools._qps import QPSStat, qps


@dataclass
class LogEvent:
    status_code: int = 200
    duration: int = 196
    uri: str = "/apache_pb.gif"
    method: str = "GET"
    timestamp: datetime = datetime(2000, 10, 9, 13, 55, 35, tzinfo=timezone.utc)
    remote_host: str = IPv4Address("218.195.205.6")


class TestQPS(unittest.TestCase):
    def __init__(self, methodName="runTest"):
        super().__init__(methodName)
        self.addTypeEqualityFunc(QPSStat, self.assertQPSEqual)

    def assertQPSEqual(self, stats1, stats2, msg=None):
        self.assertEqual(
            stats1.start_time, stats2.start_time, f"start_time differ in element {msg}"
        )
        self.assertEqual(
            stats1.end_time, stats2.end_time, f"end_time differ in element {msg}"
        )
        self.assertEqual(
            stats1.num_samples,
            stats2.num_samples,
            f"num_samples differ in element {msg}",
        )
        self.assertAlmostEqual(
            stats1.qps, stats2.qps, msg=f"qps differ in element {msg}", delta=0.01
        )

    def assertQPSListEqual(self, stats1, stats2, msg=None):
        if len(stats1) != len(stats2):
            self.fail(
                f"Lists differ in size: {len(stats1)} and {len(stats2)} respectly"
            )
        for i, (qps_stats1, qps_stats2) in enumerate(zip(stats1, stats2)):
            self.assertEqual(qps_stats1, qps_stats2, str(i))

    def test_qps(self):
        ts1 = datetime(2000, 10, 9, 13, 45, 35, tzinfo=timezone.utc)
        ts2 = datetime(2000, 10, 9, 13, 46, 35, tzinfo=timezone.utc)
        logevents = [LogEvent(timestamp=ts1), LogEvent(timestamp=ts2)]
        self.assertQPSListEqual(
            qps(logevents, timedelta(minutes=2)),
            [QPSStat(start_time=ts1, end_time=ts2, num_samples=2, qps=0.016)],
        )
        logevents = [LogEvent(timestamp=ts1)] * 120
        self.assertQPSListEqual(
            qps(logevents, timedelta(minutes=1)),
            [QPSStat(start_time=ts1, end_time=ts1, num_samples=120, qps=2.0)],
        )
        logevents = [LogEvent(timestamp=ts1)] * 60 + [LogEvent(timestamp=ts2)] * 60
        self.assertQPSListEqual(
            qps(logevents, timedelta(minutes=1)),
            [
                QPSStat(start_time=ts1, end_time=ts1, num_samples=60, qps=1.0),
                QPSStat(start_time=ts2, end_time=ts2, num_samples=60, qps=1.0),
            ],
        )
