# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from datetime import datetime, timezone
import unittest

from logtools._w3cextlog import W3CExtLogParser


class TestW3CExtLog(unittest.TestCase):
    def test_logevent(self):
        line = "2019-10-15 22:00:03 10.0.0.1 GET /foo/bar/page.asp - 80 - 1.2.3.4 Mozilla/5.0+(X11;+Linux+x86_64;+rv:60.0)+Gecko/20100101+Firefox/60.0 200 0 0 108"
        event = W3CExtLogParser().parse_line(line)
        self.assertEqual(
            event.timestamp, datetime(2019, 10, 15, 22, 0, 3, tzinfo=timezone.utc)
        )
        self.assertEqual(event.server_host, "10.0.0.1")
        self.assertEqual(event.method, "GET")
        self.assertEqual(event.uri, "/foo/bar/page.asp")
        self.assertEqual(event.query, "-")
        self.assertEqual(event.port, 80)
        self.assertEqual(event.userid, "-")
        self.assertEqual(event.remote_host, "1.2.3.4")
        self.assertEqual(
            event.user_agent,
            "Mozilla/5.0+(X11;+Linux+x86_64;+rv:60.0)+Gecko/20100101+Firefox/60.0",
        )
        self.assertEqual(event.status_code, 200)
        self.assertEqual(event.sc_substatus, 0)
        self.assertEqual(event.sc_win32_status, 0)
        self.assertEqual(event.duration, 108)
        self.assertEqual(str(event), line)
