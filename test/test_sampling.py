# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

import unittest

from logtools._sample import algorithm_L, algorithm_R, random_sample


class TestSample(unittest.TestCase):

    def test_algorithm_L(self):
        sample = algorithm_L
        self.assertEqual(sample([], 1), [])
        self.assertEqual(len(sample([1, 2, 3], 1)), 1)
        self.assertEqual(sample([1, 2, 3], 3), [1, 2, 3])

    def test_algorithm_R(self):
        sample = algorithm_R
        self.assertEqual(sample([], 1), [])
        self.assertEqual(len(sample([1, 2, 3], 1)), 1)
        self.assertEqual(sample([1, 2, 3], 3), [1, 2, 3])

    def test_sample(self):
        sample = random_sample
        self.assertRaises(ValueError, sample, [], 1)
        self.assertEqual(len(sample([1, 2, 3], 1)), 1)
        self.assertEqual(len(sample([1, 2, 3], 3)), 3)
        self.assertEqual(len(sample(iter([1, 2, 3]), 3)), 3)
