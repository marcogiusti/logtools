# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from argparse import ArgumentParser
from contextlib import closing
import sys

from ._scripts import (
    Argv,
    ExitVal,
    assert_has_fields,
    common_args_parser,
    entry_point,
    make_events_from_args,
    outstream_t,
)
from ._stats import generate_stats_text, generate_stats_csv


@entry_point
def main(argv: Argv) -> ExitVal:
    parser = ArgumentParser(
        parents=[common_args_parser()],
        description="""
            Generate a report with the statistics of the log events.
            The statistics include duration times with mean and stdev,
            most common pages, most pages with errors and most expensive
            pages in cumulated time. If the optionn --group is used, the
            output includes one line per group.""",
    )
    parser.add_argument(
        "-n",
        type=int,
        default=5,
        dest="max_num_entries",
        metavar="N",
        help="""
            Inluclude at most N entries for most common pages, most
            expensive pages and pages with most error. Text format only.
            Default: 5.""",
    )
    parser.add_argument(
        "--group",
        default=None,
        metavar="FIELD",
        help="""
            Group the log events using this attribute. Typical
            attributes are server_host, method, uri, remote_host or
            status_code. Other attibutes can be used, depending on the
            log parser used and the log type returned. CSV format
            only.""",
    )
    parser.add_argument(
        "--output",
        type=outstream_t,
        metavar="FILE",
        default=sys.stdout,
        help="""
            Save the stats to a file. If "-", dash, print to stdout.
            Default: stdout.""",
    )
    parser.add_argument(
        "--format",
        default="text",
        choices=["csv", "text"],
        help="Output format. Default: text.",
    )
    args = parser.parse_args(argv)
    assert_has_fields(args.log_parser, ["timestamp", "duration", "uri", "status_code"])
    if args.group is not None:
        assert_has_fields(args.log_parser, [args.group])
    with make_events_from_args(args) as logsiter:
        logevents = list(logsiter)
    if not logevents:
        print("No logs")
    elif args.format == "text":
        stats = generate_stats_text(logevents, args.max_num_entries)
        with closing(args.output) as fp:
            fp.write(stats)
    elif args.format == "csv":
        import csv

        include_header = not args.without_header
        with closing(args.output) as fp:
            writer = csv.writer(fp)
            for row in generate_stats_csv(logevents, args.group, include_header):
                writer.writerow(row)
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
