# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

import time
from datetime import datetime, date, timedelta, timezone
from ipaddress import IPv4Address, IPv4Network
from typing import Union


def positive_int(value: str) -> int:
    i = int(value)
    if i <= 0:
        raise ValueError()
    return i


def ipaddr(address: str) -> Union[IPv4Address, IPv4Network]:
    """
    Convert a string to an IP address or IP network.
    """

    try:
        return IPv4Address(address)
    except ValueError:
        try:
            return IPv4Network(address)
        except ValueError:
            raise ValueError(f"{address} is not an ip address neither a network")


def isodate(value: str) -> date:
    """
    Convert a string to a date.
    """

    return date.fromisoformat(value)


def isodatetime(value: str) -> datetime:
    """
    Convert a string to a timezone aware datetime. If the given value
    represents a naive datetime, assume current localtime.
    """

    dt = datetime.fromisoformat(value)
    isnaive = dt.tzinfo is None or dt.tzinfo.utcoffset(dt) is None
    if isnaive:
        # Use time module to guess the local timezone.
        # Not sure it will always work
        epoch = datetime(1970, 1, 1)
        timestamp = (dt.replace(tzinfo=None) - epoch).total_seconds()
        isdst = time.localtime(timestamp + time.timezone).tm_isdst
        if time.daylight and isdst:
            offset = timedelta(seconds=-time.altzone)
        else:
            offset = timedelta(seconds=-time.timezone)
        dt = dt.replace(tzinfo=timezone(offset))
    return dt
