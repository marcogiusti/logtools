# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from __future__ import annotations

from collections.abc import Iterable, Iterator
from dataclasses import dataclass, fields
from datetime import datetime, timedelta, timezone
from functools import partial
from itertools import chain
import re
from typing import Any, TextIO, Union

from .interface import LogEvent, LogParser, SampleFunc


# The time in the format %d/%b/%Y:%H:%M:%S %z
TIME_FORMAT_RE = (
    r"(?P<day>\d{2})"
    r"/"
    r"(?P<month>\w{3})"
    r"/"
    r"(?P<year>\d{4})"
    r":"
    r"(?P<hour>\d{2})"
    r":"
    r"(?P<minute>\d{2})"
    r":"
    r"(?P<second>\d{2})"
    r" "
    r"(?P<timezone_sign>[+-])"
    r"(?P<timezone_hours>\d{2})"
    r"(?P<timezone_minutes>\d{2})"
)
# "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\""
COMBINED_FORMAT_RE = re.compile(
    r"(?P<remote_host>\S+)"  # This is the IP address of the client
    r"\s+"
    r"(?P<ident>\S+)"  # RFC 1413 identity of the client determined by identd
    # on the clients machine
    r"\s+"
    r"(?P<userid>\S+)"  # This is the userid of the person requesting the
    # document as determined by HTTP authentication
    r"\s+"
    r"\[(?P<time>" + TIME_FORMAT_RE + r")\]"
    r"\s+"
    r'"'
    r"(?P<http_method>\S+)"
    r"\s+"
    r"(?P<http_uri>.+?)"
    r"\s+"
    r"(?P<http_version>\S+)"
    r'"'
    r"\s+"
    r"(?P<status_code>\d{3})"
    r"\s+"
    r"(?P<size>\d+)"
    r"\s+"
    r'"(?P<referer>[^"]*)"'  # Referer HTTP request header
    r"\s+"
    r'"(?P<user_agent>[^"]*)"$'  # User-Agent HTTP request header
)
MONTHS = {
    "Jan": 1,
    "Feb": 2,
    "Mar": 3,
    "Apr": 4,
    "May": 5,
    "Jun": 6,
    "Jul": 7,
    "Aug": 8,
    "Sep": 9,
    "Oct": 10,
    "Nov": 11,
    "Dec": 12,
}


class CombinedLogParser(LogParser):
    """combinedlog

    Log parser for files compatible with the Combined Log Format as
    described in https://httpd.apache.org/docs/2.4/logs.html.

    Arguments:
        None
    """

    def parse_logs(
        self,
        files: Iterable[TextIO],
        tz: timezone = timezone.utc,
        sample: Union[SampleFunc[str], None] = None,
    ) -> Iterator[CombinedLogEvent]:
        parse_line = partial(self.parse_line, tz=tz)
        lines = chain.from_iterable(files)
        if sample is not None:
            return map(parse_line, sample(lines))
        else:
            return map(parse_line, lines)

    def parse_line(self, line: str, tz: timezone = timezone.utc) -> CombinedLogEvent:
        matchobj = COMBINED_FORMAT_RE.match(line)
        if matchobj is None:
            raise ValueError("invalid line format")
        offset = timedelta(
            hours=int(matchobj.group("timezone_hours")),
            minutes=int(matchobj.group("timezone_minutes")),
        )
        if matchobj.group("timezone_sign") == "-":
            offset = -offset
        month_name = matchobj.group("month")
        try:
            month = MONTHS[month_name]
        except KeyError:
            raise ValueError(f"invalid month {month_name}")
        timestamp = datetime(
            year=int(matchobj.group("year")),
            month=month,
            day=int(matchobj.group("day")),
            hour=int(matchobj.group("hour")),
            minute=int(matchobj.group("minute")),
            second=int(matchobj.group("second")),
            tzinfo=timezone(offset),
        )
        query: Union[str, None]
        uri, qm, query = matchobj.group("http_uri").partition("?")
        if qm == query == "":
            # The separator has not been found, all the group is the URI
            # only
            query = None
        return CombinedLogEvent(
            remote_host=matchobj.group("remote_host"),
            identity=matchobj.group("ident"),
            userid=matchobj.group("userid"),
            timestamp=timestamp,
            method=matchobj.group("http_method"),
            uri=uri,
            query=query,
            protocol=matchobj.group("http_version"),
            status_code=int(matchobj.group("status_code")),
            size=int(matchobj.group("size")),
            referer=matchobj.group("referer"),
            user_agent=matchobj.group("user_agent"),
            _line=line.rstrip("\n"),
        )

    def serialize_logs(  # type: ignore[override]
        self, logevents: Iterable[CombinedLogEvent], outfile: TextIO
    ) -> None:
        for event in logevents:
            print(event, file=outfile)

    def has_field(self, name: str) -> bool:
        return name in (field.name for field in fields(CombinedLogEvent))


@dataclass
class CombinedLogEvent(LogEvent):
    # See https://httpd.apache.org/docs/current/logs.html

    __slots__ = (
        "remote_host",
        "identity",
        "userid",
        "timestamp",
        "method",
        "uri",
        "query",
        "protocol",
        "status_code",
        "size",
        "referer",
        "user_agent",
        "_line",
    )

    remote_host: str
    identity: str
    userid: str
    timestamp: datetime
    method: str
    uri: str
    query: Union[str, None]
    protocol: str
    status_code: int
    size: int
    referer: str
    user_agent: str
    _line: Union[str, None]

    def replace(self, **kwargs: Any) -> CombinedLogEvent:
        for name, value in kwargs.items():
            setattr(self, name, value)
        self._line = None
        return self

    def __str__(self) -> str:
        if self._line is not None:
            return self._line
        else:
            if self.query is not None:
                uri = f"{self.uri}?{self.query}"
            else:
                uri = self.uri
            return (
                f"{self.remote_host} {self.identity} {self.userid} "
                f"[{self.timestamp:%d/%b/%Y:%H:%M:%S %z}] "
                f'"{self.method} {uri} {self.protocol}" '
                f'{self.status_code} {self.size} "{self.referer}" '
                f'"{self.user_agent}"'
            )
