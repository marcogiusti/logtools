# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from argparse import ArgumentParser
import sys

from ._converters import ipaddr, isodate, isodatetime, positive_int
from ._filters import (
    timestamp_gte,
    timestamp_lte,
    file_extension_eq,
    is_static_file,
    method_eq,
    remote_host_eq,
    slower_than,
    status_code_eq,
    uri_eq,
)
from ._scripts import (
    Argv,
    ExitVal,
    assert_has_field,
    entry_point,
    logfilter_t,
    make_events_from_args,
    serialize_log_events_from_args,
    stream_common_args_parser,
)
from .interface import LogEvent


@entry_point
def main(argv: Argv) -> ExitVal:
    parser = ArgumentParser(
        parents=[stream_common_args_parser()],
        description="""
            Read the log events from the given files or the standard input
            and filter out all the lines that do not match the given
            filters. If the --exclude option is used, filter out all the
            events that match at least one parameter.""",
    )
    parser.add_argument(
        "--exclude",
        action="store_true",
        help="""
            Invert the filters and exclude all the lines that match at least
            one filter.""",
    )
    filters_group = parser.add_argument_group("Available filters")
    filters_group.add_argument(
        "--from",
        type=isodatetime,
        metavar="DATETIME",
        help="""
            Filter events after the given date and time (in iso format,
            YYYY-MM-DD[Thh[:mm[:ss[+hh:mm]]]]).""",
    )
    filters_group.add_argument(
        "--thru",
        type=isodatetime,
        metavar="DATETIME",
        help="""
            Filter events before the given date and time (in iso format,
            YYYY-MM-DD[Thh[:mm[:ss[+hh:mm]]]]).""",
    )
    filters_group.add_argument(
        "--day",
        type=isodate,
        help="Filter events in the given day (date in iso format, YYYY-MM-DD).",
    )
    filters_group.add_argument(
        "--slower-than",
        type=positive_int,
        dest="duration_threshold",
        metavar="MSEC",
        help="""
            Filter events with duration time greater than the given parameter
            in milliseconds.""",
    )
    filters_group.add_argument(
        "--ip",
        type=ipaddr,
        action="append",
        default=[],
        help="""
            If the argument is an IP address, filter all the requests from
            this IP address, if the argument is an IP network, filter all
            the requests beloning to this network. This option can be used
            multiple times. Accepts only IPv4 addresses and networks. Ex.
            192.168.1.10 or 10.0.0.0/8.""",
    )
    filters_group.add_argument(
        "--status-code",
        type=status_code_eq,
        action="append",
        default=[],
        help="""
            Filter the requests matching the status code. Status code can be
            either a numeric (between 100 and 599) or a specific class of
            status codes: 1xx, 2xx, 3xx, 4xx or 5xx. This option can be used
            multiple times.""",
    )
    filters_group.add_argument(
        "--method",
        action="append",
        default=[],
        help="""
            Filter the requests matching the given method. This option can be
            used multiple times.""",
    )
    filters_group.add_argument(
        "--uri",
        action="append",
        default=[],
        help="""
            Filter the requests which the uri is the same as the given
            parameter. This option can be used multiple times.""",
    )
    filters_group.add_argument(
        "--file-type",
        action="append",
        default=[],
        metavar="EXTENSION",
        help="""
            Filter the requests with resource file extension equal to the
            given parameter. This option can be used multiple times.""",
    )
    filters_group.add_argument(
        "--static-files",
        action="store_true",
        help="""
            Filter the requests to static files. Some common static files are
            filtered: css, gif, htm, html, ico, jpg, jpeg, js, pdf, png, svg,
            swf, txt, xml, zip.""",
    )
    filters_group.add_argument(
        "--custom-filter",
        action="append",
        type=logfilter_t,
        default=[],
        metavar="FILTER_FACTORY",
        help="""
            Use a custom filter. The factory must return an instance of
            LogFilter. This option can be used multiple times.
            Format: [package.]module.filter_factory[:params].""",
    )
    args = parser.parse_args(argv)
    filters = []
    from_date = getattr(args, "from")
    thru_date = args.thru
    if args.day is not None:
        if from_date is not None and from_date.date() >= args.day:
            from_date = args.day
        elif from_date is None:
            from_date = args.day
        if thru_date is not None and thru_date.date() <= args.day:
            thru_date = args.day
        elif thru_date is None:
            thru_date = args.day
    if args.duration_threshold is not None:
        assert_has_field(args.log_parser, "duration")
        filters.append(slower_than(args.duration_threshold))
    if args.status_code:
        assert_has_field(args.log_parser, "status_code")
        for status in args.status_code:
            filters.append(status)
    if args.uri:
        assert_has_field(args.log_parser, "uri")
        for uri in args.uri:
            filters.append(uri_eq(uri))
    if args.method:
        assert_has_field(args.log_parser, "method")
        for method in args.method:
            filters.append(method_eq(method))
    if from_date is not None:
        assert_has_field(args.log_parser, "timestamp")
        filters.append(timestamp_gte(from_date))
    if thru_date is not None:
        assert_has_field(args.log_parser, "timestamp")
        filters.append(timestamp_lte(thru_date))
    if args.ip:
        assert_has_field(args.log_parser, "remote_host")
        for ip in args.ip:
            filters.append(remote_host_eq(ip))
    if args.file_type:
        assert_has_field(args.log_parser, "uri")
        filters.append(file_extension_eq(args.file_type))
    if args.static_files:
        assert_has_field(args.log_parser, "uri")
        filters.append(is_static_file())
    filters.extend(args.custom_filter)
    if args.exclude:

        def filter_func(logevent: LogEvent) -> bool:
            return not any(f(logevent) for f in filters)

    else:

        def filter_func(logevent: LogEvent) -> bool:
            return all(f(logevent) for f in filters)

    with make_events_from_args(args) as logsiter:
        filtered = filter(filter_func, logsiter)
        serialize_log_events_from_args(filtered, args)
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
