# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from __future__ import annotations
from argparse import (
    _ArgumentGroup,
    SUPPRESS,
    Action,
    ArgumentParser,
    ArgumentTypeError,
    Namespace,
)
from collections.abc import Callable, Iterable, Iterator, Sequence
from contextlib import closing, contextmanager
from datetime import timedelta, timezone
from functools import wraps
from io import TextIOWrapper
from itertools import takewhile
import sys
from types import TracebackType
from typing import Any, Generator, NoReturn, TextIO, Union, cast

from ._import import InvalidSpecError, iter_entry_points, resolve_spec
from ._sample import algorithm_L
from ._w3cextlog import W3CExtLogParser
from .interface import (
    LOGFILTER_ENTRY_POINTS_GROUP,
    LOGPARSER_ENTRY_POINTS_GROUP,
    SAMPLE_ALGORITHM_ENTRY_POINTS_GROUP,
    LogEvent,
    LogFilter,
    LogParser,
    SampleAlgorithm,
)


Argv = Sequence[str]
ExitVal = Union[int, str]
MainFunc = Callable[[Argv], ExitVal]
MainFuncEntryPoint = Callable[[Union[Argv, None]], NoReturn]


def logfilter_t(value: str) -> LogFilter:
    try:
        log_filter = resolve_spec(value, LOGFILTER_ENTRY_POINTS_GROUP)
    except (ImportError, InvalidSpecError, TypeError) as exc:
        raise ArgumentTypeError(str(exc))
    if not callable(log_filter):
        raise ArgumentTypeError(f"object {log_filter!r} is not a valid filter")
    return cast(LogFilter, log_filter)


def logparser_t(value: str) -> LogParser:
    try:
        parser = resolve_spec(value, LOGPARSER_ENTRY_POINTS_GROUP)
    except (ImportError, InvalidSpecError, TypeError) as exc:
        raise ArgumentTypeError(str(exc))
    if not isinstance(parser, LogParser):
        raise ArgumentTypeError(f"object {value!r} is not a valid parser")
    return parser


def sample_algorithm_t(value: str) -> SampleAlgorithm[Any]:
    try:
        func = resolve_spec(value, SAMPLE_ALGORITHM_ENTRY_POINTS_GROUP)
    except (ImportError, InvalidSpecError, TypeError) as exc:
        raise ArgumentTypeError(str(exc))
    if not callable(func):
        raise ArgumentTypeError(f"object {func!r} is not a valid sample algorithm")
    return cast(SampleAlgorithm[Any], func)


def timezone_t(value: str) -> timezone:
    return timezone(timedelta(hours=int(value)))


def timedelta_t(param: str) -> timedelta:
    try:
        value, uom = param.split(maxsplit=1)
    except ValueError:
        itr = iter(param)
        value = "".join(takewhile(str.isdigit, itr))
        uom = param[len(value) :]
    try:
        ivalue = int(value)
    except ValueError:
        raise ArgumentTypeError("Invalid interval")

    if uom == "s":
        uom = "seconds"
    elif uom == "m":
        uom = "minutes"
    elif uom == "h":
        uom = "hours"
    elif uom == "d":
        uom = "days"

    if ivalue == 1 and not uom.endswith("s"):
        uom = uom + "s"
    try:
        td = timedelta(**{uom: ivalue})
    except TypeError:
        raise ArgumentTypeError("Unknown interval type")
    except OverflowError:
        raise ArgumentTypeError("Interval out of range")
    if td.total_seconds() < 0:
        raise ArgumentTypeError("Invalid negative interval")
    elif td.total_seconds() == 0:
        raise ArgumentTypeError("Invalid zero interval")
    return td


class MaybeCloseFilePointer(TextIO):
    def __init__(self, fp: TextIO, shall_close: bool):
        self.fp = fp
        self.shall_close = shall_close

    def close(self) -> None:
        if self.shall_close:
            self.fp.close()

    def __enter__(self) -> MaybeCloseFilePointer:
        return self

    def __exit__(
        self,
        exc_type: Union[type[BaseException], None],
        exc_value: Union[BaseException, None],
        traceback: Union[TracebackType, None],
    ) -> None:
        self.close()

    def __iter__(self) -> Iterator[str]:
        return iter(self.fp)

    def write(self, s: str) -> int:
        return self.fp.write(s)

    def flush(self) -> None:
        self.fp.flush()


def instream_t(name: str, encoding: Union[str, None] = None) -> TextIO:
    fp: TextIO
    shall_close = False
    if name == "-":
        if encoding is not None:
            fp = TextIOWrapper(sys.stdin.buffer, encoding=encoding)
        else:
            fp = sys.stdin
    else:
        fp = open(name, encoding=encoding, errors="replace")
        shall_close = True
    return MaybeCloseFilePointer(fp, shall_close)  # type: ignore


def outstream_t(name: str, encoding: Union[str, None] = None) -> TextIO:
    fp: TextIO
    shall_close = False
    if name == "-":
        if encoding is not None:
            fp = TextIOWrapper(sys.stdout.buffer, encoding=encoding)
        else:
            fp = sys.stdout
    else:
        fp = open(name, mode="wt", encoding=encoding, errors="replace")
        shall_close = True
    return MaybeCloseFilePointer(fp, shall_close)  # type: ignore


def infile_t(name: str, encoding: Union[str, None] = None) -> TextIO:
    fp = open(name, encoding=encoding, errors="replace")
    return fp


def outfile_t(name: str, encoding: Union[str, None] = None) -> TextIO:
    fp = open(name, mode="wt", encoding=encoding, errors="replace")
    return fp


def sample_size_t(value: str) -> int:
    i = int(value)
    if i < 0:
        raise ValueError("invalid negative sample")
    return i


def plugin_help_action(entry_points_group: str) -> type[Action]:
    class HelpAction(Action):
        def __init__(
            self,
            option_strings: Sequence[str],
            dest: str = SUPPRESS,
            default: str = SUPPRESS,
            help: Union[str, None] = None,
        ):
            super().__init__(
                option_strings=option_strings,
                dest=dest,
                default=default,
                nargs=0,
                help=help,
            )

        def __call__(
            self,
            argparser: ArgumentParser,
            namespace: Namespace,
            values: Union[str, Sequence[Any], None],
            option_string: Union[str, None] = None,
        ) -> None:
            for ep in iter_entry_points(group=entry_points_group):
                try:
                    obj = ep.load()
                    print(obj.__doc__, end="\n\n")
                except Exception:
                    continue

            argparser.exit()

    return HelpAction


def _common_args_parser() -> tuple[ArgumentParser, _ArgumentGroup]:
    parser = ArgumentParser(add_help=False)
    parser.add_argument(
        "-v", "--verbose", action="store_true", help="Show errors on parsing log lines."
    )
    parser.add_argument(
        "--help-parsers",
        action=plugin_help_action(LOGPARSER_ENTRY_POINTS_GROUP),
        help="Print an help text for each supported parser.",
    )
    parser.add_argument(
        "--help-sample-algorithms",
        action=plugin_help_action(SAMPLE_ALGORITHM_ENTRY_POINTS_GROUP),
        help="Print an help text for each supported sampling algorithm.",
    )
    common_group = parser.add_argument_group("Common arguments")
    common_group.add_argument(
        "--log-parser",
        metavar="SPEC",
        type=logparser_t,
        default=W3CExtLogParser(),
        help="""
            Use custom parser to parse the log lines. The parser must
            return an instance of LogEvent for every log entry.
            Format: [package.]module.attr[:params]. Use --help-parsers to see a
            list of available parsers and the accepted options.
            Default: logtools.W3CExtLogEvent.""",
    )
    common_group.add_argument(
        "--timezone",
        type=timezone_t,
        metavar="[+-]HH",
        default=timezone.utc,
        help="""
            Adjustment of creation time of the logs as offset in hours.
            Default: 0 (UTC).""",
    )
    common_group.add_argument("--input-encoding", help="Encoding of input files.")
    common_group.add_argument(
        "--sample-algorithm",
        type=sample_algorithm_t,
        default=algorithm_L,
        help="Sampling algorithm. Default: algorithm_L",
    )
    common_group.add_argument(
        "-k",
        "--sample-size",
        type=sample_size_t,
        default=0,
        help="""
            Take a sample of K unique events. Sample size must be positive.
            Default: 0, take all logs.""",
    )
    parser.add_argument(
        "logfile",
        nargs="*",
        default=["-"],
        help="""
            Files that contain the logs. Use single dash to read from stdin
            Default: read from stdin.""",
    )
    return parser, common_group


def common_args_parser() -> ArgumentParser:
    return _common_args_parser()[0]


def stream_common_args_parser() -> ArgumentParser:
    parser, common_group = _common_args_parser()
    common_group.add_argument(
        "--output",
        default="-",
        help="Output file. If '-', print to stdout. Default: stdout",
    )
    common_group.add_argument("--output-encoding")
    return parser


def parse_logs(
    files: Iterable[TextIO],
    logparser: LogParser = W3CExtLogParser(),
    tz: timezone = timezone.utc,
    show_err: bool = False,
    sample_algorithm: SampleAlgorithm[str] = algorithm_L,
    sample_size: int = 0,
) -> Iterable[LogEvent]:
    """
    Parse the given files, drop the invalid logs and return an
    iterable of LogEvent from them.

    :param files: an iterable of lines containing the logs.
    :param logparser: the function used to parse the log line.
    :param tz: the timezone of the creation time of the logs.
    :param show_err: print the lines that can not be parsed to the
        stderr.
    """

    def log_factory(
        log_generator: Iterator[LogEvent],
        show_err: bool = show_err,
    ) -> Generator[Union[LogEvent, None], None, None]:
        while True:
            try:
                yield next(log_generator)
            except StopIteration:
                return
            except Exception as exc:
                if show_err:
                    print(f"invalid line\n{exc!s}", file=sys.stderr)
                yield None

    if sample_size > 0:

        def sample(population: Iterable[str]) -> list[str]:
            return sample_algorithm(population, sample_size)

        logsiter = logparser.parse_logs(files, tz, sample)
    else:
        logsiter = logparser.parse_logs(files, tz)
    return filter(None, log_factory(logsiter))


@contextmanager
def make_events_from_args(args: Namespace) -> Iterator[Iterable[LogEvent]]:
    encoding = args.input_encoding
    logfiles = [instream_t(name, encoding) for name in args.logfile]

    logsiter = parse_logs(
        files=logfiles,
        logparser=args.log_parser,
        tz=args.timezone,
        show_err=args.verbose,
        sample_algorithm=args.sample_algorithm,
        sample_size=args.sample_size,
    )
    try:
        yield logsiter
    finally:
        for input_log_file in logfiles:
            try:
                input_log_file.close()
            except Exception:
                pass


def serialize_log_events_from_args(
    logevents: Iterable[LogEvent], args: Namespace
) -> None:
    fp = outstream_t(args.output, args.output_encoding)
    logparser = args.log_parser
    with closing(fp):
        logparser.serialize_logs(logevents, fp)


def assert_has_field(logparser: LogParser, field_name: str) -> None:
    if not logparser.has_field(field_name):
        raise SystemExit(f"Invalid parser, event is missing {field_name} field")


def assert_has_fields(logparser: LogParser, field_names: Sequence[str]) -> None:
    for field_name in field_names:
        assert_has_field(logparser, field_name)


def entry_point(main_func: MainFunc) -> MainFuncEntryPoint:
    @wraps(main_func)
    def run_main(argv: Union[Argv, None] = None) -> NoReturn:
        if argv is None:
            argv = sys.argv[1:]
        try:
            sys.exit(main_func(argv))
        except (KeyboardInterrupt, BrokenPipeError):
            sys.exit(0)

    return run_main
