# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from argparse import ArgumentParser
from contextlib import closing
import csv
from datetime import datetime, timedelta
import json
import sys

from ._qps import generate_stats_csv, generate_stats_json, qps
from ._scripts import (
    Argv,
    ExitVal,
    assert_has_field,
    common_args_parser,
    entry_point,
    make_events_from_args,
    outstream_t,
    timedelta_t,
)


class JSONEncoder(json.JSONEncoder):
    def default(self, obj: object) -> object:
        if isinstance(obj, datetime):
            return obj.isoformat()
        else:
            return super().default(obj)


@entry_point
def main(argv: Argv) -> ExitVal:
    parser = ArgumentParser(
        parents=[common_args_parser()],
        description="""
            Read the log events from the given files or the standard input
            and take a sample of K unique events.""",
    )
    parser.add_argument(
        "--window-size",
        type=timedelta_t,
        default=timedelta(minutes=5),
        metavar="TIMEDELTA",
        help="""
            Interval size when to calculate the number of query per
            seconds.  Format examples: 1m, "1 minute", "5 minutes", etc.
            Known unit of measures: seconds (second, s), minutes
            (minute, m), hours (hour, h), days (day, d).
            Default: 5 minutes.""",
    )
    parser.add_argument(
        "--output",
        type=outstream_t,
        metavar="FILE",
        default=sys.stdout,
        help="""
            Save the stats to a file. If "-", dash, print to stdout.
            Default: stdout.""",
    )
    parser.add_argument(
        "--without-header",
        action="store_true",
        help="Exclude the header with fields name from the output.",
    )
    parser.add_argument(
        "--format",
        default="csv",
        choices=["csv", "json"],
        help="Output format.",
    )

    args = parser.parse_args(argv)
    assert_has_field(args.log_parser, "timestamp")
    with make_events_from_args(args) as logsiter:
        stats = qps(logsiter, args.window_size)
    if args.format == "csv":
        with closing(args.output) as fp:
            include_header = not args.without_header
            writer = csv.writer(fp)
            for row in generate_stats_csv(stats, include_header):
                writer.writerow(row)
    elif args.format == "json":
        with closing(args.output) as fp:
            json.dump(generate_stats_json(stats), fp, cls=JSONEncoder)
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
