# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from argparse import ArgumentParser
from contextlib import closing
import json
from secrets import token_urlsafe
import sys
from typing import Callable, Dict, List

from ._scripts import (
    Argv,
    ExitVal,
    assert_has_fields,
    entry_point,
    infile_t,
    make_events_from_args,
    outfile_t,
    serialize_log_events_from_args,
    stream_common_args_parser,
)
from .interface import LogEvent


@entry_point
def main(argv: Argv) -> ExitVal:
    parser = ArgumentParser(
        parents=[stream_common_args_parser()],
        description="""
            Read the log events from the given files or the standard input
            and anonymize the URL part. The new URLs are randomly chosen.
            Same URLs will result on same new random URL. The mapping can be
            saved and later on loaded again.""",
    )
    parser.add_argument(
        "--field",
        action="append",
        default=None,
        help="""
            Anonymize the given field. This option can be used multiple times,
            alternatively use a comma to separate the fields.
            Default: uri.""",
    )
    parser.add_argument(
        "--with-mapping",
        type=infile_t,
        metavar="FILE",
        dest="mapping_input_file",
        help="""Load the mapping from a file and reused for the new
            randomization process. This means that previously seen URLs will
            reuse the previous random result.""",
    )
    parser.add_argument(
        "--save-mapping",
        type=outfile_t,
        metavar="FILE",
        dest="mapping_output_file",
        help="Save the mapping from URLs to randomized URLs in a file.",
    )
    parser.add_argument(
        "--reverse",
        action="store_true",
        help="""
            Reverse the mapping, restoring the original URLs. This works only
            with the --with-mapping option.
            Default: false.""",
    )
    args = parser.parse_args(argv)
    fields = args.field
    if fields is None:
        fields = ["uri"]
    elif len(fields) == 1:
        fields = fields[0].split(",")
    assert_has_fields(args.log_parser, fields)
    if args.mapping_input_file:
        with closing(args.mapping_input_file):
            mapping = json.load(args.mapping_input_file)
            if args.reverse:
                reverse_mapping = {}
                for field_name in fields:
                    reverse_mapping[field_name] = {
                        value: key for key, value in mapping.get(field_name, {}).items()
                    }
                mapping = reverse_mapping
    else:
        mapping = {field_name: {} for field_name in fields}

    def anon_event(
        logevent: LogEvent,
        fields: List[str] = fields,
        mapping: Dict[str, Dict[str, str]] = mapping,
        token_urlsafe: Callable[[], str] = token_urlsafe,
    ) -> LogEvent:
        new_values = {}
        for field_name in fields:
            cur_value = getattr(logevent, field_name)
            field_values = mapping.setdefault(field_name, {})
            if cur_value in field_values:
                new_value = mapping[field_name][cur_value]
            else:
                field_values[cur_value] = new_value = token_urlsafe()
            new_values[field_name] = new_value
        return logevent.replace(**new_values)

    with make_events_from_args(args) as logiter:
        serialize_log_events_from_args(map(anon_event, logiter), args)
    if args.mapping_output_file is not None:
        with closing(args.mapping_output_file):
            json.dump(mapping, args.mapping_output_file)
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
