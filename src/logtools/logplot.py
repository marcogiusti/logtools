# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.
# type: ignore

import abc
import argparse
from collections.abc import Callable, Mapping, MutableSequence
from dataclasses import dataclass
from itertools import groupby
from operator import attrgetter
import sys
from typing import Any, BinaryIO, Union, cast

from matplotlib.axes import Axes
from matplotlib.artist import Artist
from matplotlib.backend_bases import FigureCanvasBase, Event, KeyEvent, PickEvent
from matplotlib.legend import Legend
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
from matplotlib.dates import AutoDateFormatter, AutoDateLocator, date2num, epoch2num
from matplotlib.text import Text

from ._scripts import (
    Argv,
    ExitVal,
    assert_has_field,
    common_args_parser,
    entry_point,
    make_events_from_args,
)
from .interface import AnyEvent


Artists = MutableSequence[Artist]
EventHandler_t = Callable[[Event], Any]


@dataclass
class PlotOptions:

    title: Union[str, None] = None


@dataclass
class XAxisOptions:
    pass


@dataclass
class YAxisOptions:

    logscale: bool = False
    group: Union[str, None] = None
    group_limit: int = 20
    interval: int = 60


class EventHandler:
    def __init__(self, artists: Artists, legend: Legend):
        self.artists = artists
        self.legend = legend
        self._legmap = dict(zip(legend.get_lines(), artists))
        self._legmap_inv = dict(zip(self._legmap.values(), self._legmap.keys()))
        self._textsmap = dict(zip(legend.get_lines(), legend.texts))

    def connect(self, canvas: FigureCanvasBase) -> None:
        canvas.mpl_connect("key_press_event", cast(EventHandler_t, self.on_key_press))
        # make the legend interactive
        for legline in self.legend.get_lines():
            legline.set_picker(10)  # 10 pts tolerance
        canvas.mpl_connect("pick_event", cast(EventHandler_t, self.on_pick))

    def toggle_visible(self, obj: Union[Artist, Legend]) -> None:
        obj.set_visible(not obj.get_visible())

    def toggle_alpha(self, obj: Text) -> None:
        # alpha_mapping: Mapping[Union[float, None], float] = {0.2: 1.0, 1.0: 0.2}
        alpha_mapping: Mapping[float, float] = {0.2: 1.0, 1.0: 0.2}
        obj.set_alpha(alpha_mapping.get(obj.get_alpha(), 0.2))

    def on_key_press(self, event: KeyEvent) -> None:
        toggle_keys = {
            "1": 0,
            "2": 1,
            "3": 2,
            "4": 3,
            "5": 4,
            "6": 5,
            "7": 6,
            "8": 7,
            "9": 8,
            "w": 9,
            "e": 10,
            "r": 11,
            "t": 12,
            "y": 13,
            "u": 14,
            "i": 15,
            "o": 16,
            "p": 17,
        }
        # toggle single plot
        if event.key in toggle_keys:
            idx = toggle_keys[event.key]
            try:
                artist = self.artists[idx]
            except IndexError:
                pass
            else:
                self.toggle_visible(artist)
                legline = self._legmap_inv[artist]
                self.toggle_alpha(self._textsmap[legline])
                event.canvas.draw()
        # toggle all plots
        elif event.key == "0":
            visible = not all(a.get_visible() for a in self.artists)
            for artist in self.artists:
                artist.set_visible(visible)
            alpha = 1.0 if visible else 0.2
            for text in self.legend.texts:
                text.set_alpha(alpha)
            event.canvas.draw()
        # toggle legend
        elif event.key == "-":
            self.toggle_visible(self.legend)
            event.canvas.draw()

    def on_pick(self, event: PickEvent) -> None:
        # on the pick event, find the orig line corresponding to the
        # legend proxy line, and toggle the visibility
        legline = event.artist
        try:
            origline = self._legmap[legline]
        except KeyError:
            # not a legend line
            return
        vis = origline.get_visible()
        origline.set_visible(not vis)
        # Change the alpha on the line in the legend so we can see what lines
        # have been toggled
        self.toggle_alpha(self._textsmap[legline])
        if vis:
            legline.set_alpha(0.2)
        else:
            legline.set_alpha(1.0)
        event.canvas.draw()


class IPlot(abc.ABC):
    @abc.abstractmethod
    def setup(
        self,
        logevents: list[AnyEvent],
        plotopts: PlotOptions,
        xaxis_opts: XAxisOptions,
        yaxis_opts: YAxisOptions,
        plot2_type: Union[str, None],
        yaxis2_opts: YAxisOptions,
    ) -> tuple[Artists, Legend]:
        pass

    @abc.abstractmethod
    def setup_x_axes(
        self, axes: Axes, logevents: list[AnyEvent], opts: XAxisOptions
    ) -> None:
        pass

    @abc.abstractmethod
    def setup_y_axes(
        self, axes: Axes, logevents: list[AnyEvent], opts: YAxisOptions
    ) -> Artists:
        pass

    @abc.abstractmethod
    def setup_legend(self, axes: Axes, artists: Artists) -> Legend:
        pass


class PlotBase(IPlot):
    def setup_x_axes(
        self, axes: Axes, logevents: list[AnyEvent], opts: XAxisOptions
    ) -> None:

        # xlim_min = min(events[0].timestamp for events in logevents_files)
        # xlim_max = max(events[-1].timestamp for events in logevents_files)

        xlabel = "time"
        locator = AutoDateLocator(minticks=5, maxticks=10)
        formatter = AutoDateFormatter(locator)
        formatter.scaled = {
            365.0: "%Y",
            30.0: "%b %Y",
            1.0: "%b %d %Y",
            1.0 / 24.0: "%b %d %Y\n%H:%M:%S",
            1.0 / (24.0 * 60.0): "%b %d %Y\n%H:%M:%S",
        }
        axes.set_xlabel(xlabel)
        axes.set_xticklabels(axes.get_xticks(), rotation=90, fontsize=9)
        axes.get_xaxis().set_major_locator(locator)
        axes.get_xaxis().set_major_formatter(formatter)
        # axes.set_xlim(date2num([xlim_min, xlim_max]))

    def setup_legend(self, axes: Axes, artists: Artists) -> Legend:
        legend = axes.legend(
            artists,
            [str(artist.get_label()) for artist in artists],
            loc="upper left",
            frameon=False,
            numpoints=1,
            fontsize=9,
            # fancybox=True,
            # shadow=True
        )
        # legend.get_frame().set_alpha(0.4)
        return legend

    def setup(
        self,
        logevents: list[AnyEvent],
        plotopts: PlotOptions,
        xaxis_opts: XAxisOptions,
        yaxis_opts: YAxisOptions,
        plot2_type: Union[str, None],
        yaxis2_opts: YAxisOptions,
    ) -> tuple[Artists, Legend]:

        plt.figure(figsize=(12, 8), dpi=100, facecolor="w", edgecolor="w")
        axes = plt.subplot(111)
        axes.grid(True)
        if plotopts.title is not None:
            axes.set_title(plotopts.title)
        self.setup_x_axes(axes, logevents, xaxis_opts)
        artists = self.setup_y_axes(axes, logevents, yaxis_opts)
        if plot2_type is not None:
            axis2 = axes.twinx()
            plot2 = make_plot(plot2_type)
            artists.extend(plot2.setup_y_axes(axis2, logevents, yaxis2_opts))
        legend = self.setup_legend(axes, artists)
        return artists, legend


class ScatterPlot(PlotBase):
    def _plot(self, axes: Axes, logevents: list[AnyEvent], label: str) -> list[Line2D]:

        x = date2num([event.timestamp for event in logevents])
        # color, marker = self.color_map(group)
        y = [event.duration for event in logevents]

        # color, marker = self.color_map(group)
        # group_label = group.replace('$', '\$')
        # artist = axes.plot_date(x, y, color=color, markeredgecolor='k',
        #                         marker=marker, alpha=0.8,
        #                         markersize=7, picker=5, label=group_label)[0]
        artists = axes.plot_date(
            x, y, markeredgecolor="k", alpha=0.8, markersize=7, picker=5, label=label
        )
        return artists

    def setup_y_axes(
        self, axes: Axes, logevents: list[AnyEvent], opts: YAxisOptions
    ) -> Artists:

        ylabel = "duration in ms"
        artists: Artists = []

        if opts.logscale:
            ylabel += " (log scale)"
            plt.gca().set_yscale("log")
            axes.semilogy()
        else:
            plt.gca().set_yscale("linear")

        axes.set_ylabel(ylabel)

        if opts.group is None:
            new_artists = self._plot(
                axes, logevents, label=f"Total events: {len(logevents)}"
            )
            artists.extend(new_artists)
        else:
            logevents.sort(key=attrgetter(opts.group))
            groups = {
                grp: list(evs)
                for grp, evs in groupby(logevents, key=attrgetter(opts.group))
            }
            others: list[AnyEvent] = []
            group_keys = sorted(
                groups.keys(), key=lambda k: len(groups[k]), reverse=True
            )
            for i, key in enumerate(group_keys):
                if i < opts.group_limit:
                    artists.extend(
                        self._plot(
                            axes,
                            logevents=groups[key],
                            label=f"{key} ({len(groups[key])})",
                        )
                    )
                else:
                    others += groups[key]
            if others:
                artists.extend(
                    self._plot(axes, logevents=others, label=f"others ({len(others)})")
                )
        return artists


class LinePlot(PlotBase):
    def setup_y_axes(
        self, axes: Axes, logevents: list[AnyEvent], opts: YAxisOptions
    ) -> Artists:

        ylabel = "Number or requests"
        if opts.logscale:
            ylabel += " (log scale)"
            plt.gca().set_yscale("log")
            axes.semilogy()
        else:
            plt.gca().set_yscale("linear")
        axes.set_ylabel(ylabel)

        def interval_key(ev: AnyEvent) -> int:
            v = (ev.timestamp.timestamp() // opts.interval) * opts.interval
            return int(v)

        logevents.sort(key=attrgetter("timestamp"))
        data = {
            group_key: sum(1 for _ in events)
            for group_key, events in groupby(logevents, key=interval_key)
        }
        x = []
        y = []
        timestamp = interval_key(logevents[0])
        stop = interval_key(logevents[-1]) + opts.interval
        while timestamp <= stop:
            x.append(timestamp)
            y.append(data.get(timestamp, 0))
            timestamp += opts.interval
        artists = axes.plot(epoch2num(x), y, alpha=0.8, label="Number of requests")
        return artists


def make_plot(plot_type: str) -> IPlot:
    if plot_type == "scatter":
        return ScatterPlot()
    if plot_type == "line":
        return LinePlot()
    else:
        raise ValueError(f"invalid plot type {plot_type}")


@entry_point
def main(argv: Argv) -> ExitVal:
    parser = argparse.ArgumentParser(
        parents=[common_args_parser()],
        description="""
            Visualize log events in different plot types.

            Available plot types: scatter plot and line plot.
            """,
    )
    parser.add_argument(
        "--save-plot",
        metavar="FILE",
        help="""Save the plot to a file. Format is chosen by extension.
            Default format: PNG.""",
    )
    parser.add_argument(
        "--overwrite",
        action="store_true",
        help="""If the file exists, overwrite it. By default logplot refuses to
            override the file.""",
    )
    mainaxis_parser = parser.add_argument_group("Main plot parameters")
    mainaxis_parser.add_argument(
        "--type",
        choices=["scatter", "line"],
        default="scatter",
        help="Choose the type of plot. Default: scatter.",
    )
    mainaxis_parser.add_argument(
        "--logscale",
        action="store_true",
        help="Show Y axis in logarithmic scale. Default is linear scale.",
    )
    mainaxis_parser.add_argument("--title", help="Set the title of the chart.")
    mainaxis_parser.add_argument(
        "--group",
        default=None,
        metavar="FIELD",
        help="""
            Group the log events using this attribute. Typical
            attributes are server_host, method, uri, remote_host or
            status_code. Other attibutes can be used, depending on the
            log parser used and the log type returned.""",
    )
    mainaxis_parser.add_argument(
        "--group-limit",
        type=int,
        metavar="N",
        default=20,
        help="Create at most N groups plus one for all the others events.",
    )
    mainaxis_parser.add_argument(
        "--interval",
        type=int,
        default=60,
        help="""
            Interval length in seconds used to count the number of
            requests in the line plot. Default: 60 seconds.""",
    )
    auxaxis_parser = parser.add_argument_group("Secondary plot parameters")
    auxaxis_parser.add_argument(
        "--plot2-type",
        choices=["scatter", "line"],
        default=None,
        help="""
            Select the type of plot to use in the second Y axis. Default
            no plot.""",
    )
    auxaxis_parser.add_argument(
        "--plot2-logscale",
        action="store_true",
        help="""Show second Y axis in logarithmic scale. Default is
            linear scale.""",
    )
    auxaxis_parser.add_argument(
        "--plot2-group",
        default=None,
        metavar="FIELD",
        help="""
            Group the log events in the second Y axis using this attribute.
            Typical attributes are server_host, method, uri, remote_host or
            status_code. Other attibutes can be used, depending on the log
            parser used and the log type returned.""",
    )
    auxaxis_parser.add_argument(
        "--plot2-group-limit",
        type=int,
        metavar="N",
        default=20,
        help="""
            Create in the second Y axis at most N groups plus one for
            all the others events.""",
    )
    auxaxis_parser.add_argument(
        "--plot2-interval",
        type=int,
        metavar="INTERVAL",
        default=60,
        help="""
            Interval length in seconds used to count the number of
            requests in the line plot. Default: 60 [seconds].""",
    )
    args = parser.parse_args(argv)

    assert_has_field(args.log_parser, "timestamp")
    assert_has_field(args.log_parser, "duration")
    if args.group is not None:
        assert_has_field(args.log_parser, args.group)
    plot = make_plot(args.type)
    with make_events_from_args(args) as logsiter:
        logevents = list(logsiter)
    if not logevents:
        return "Nothing to plot"

    plotopts = PlotOptions(title=args.title)
    xaxis_opts = XAxisOptions()
    yaxis_opts = YAxisOptions(
        logscale=args.logscale,
        group=args.group,
        group_limit=args.group_limit,
        interval=args.interval,
    )
    yaxis2_opts = YAxisOptions(
        logscale=args.plot2_logscale,
        group=args.plot2_group,
        group_limit=args.plot2_group_limit,
        interval=args.plot2_interval,
    )

    if args.save_plot is not None:
        mode = "wb" if args.overwrite else "xb"
        try:
            # Do not parse all the logs and try to plot them if we cannot write
            # to the output file
            with open(args.save_plot, mode) as fp:
                plt.switch_backend("agg")
                plot.setup(
                    logevents,
                    plotopts,
                    xaxis_opts,
                    yaxis_opts,
                    args.plot2_type,
                    yaxis2_opts,
                )
                plt.savefig(cast(BinaryIO, fp))
        except FileExistsError:
            return f'File "{args.save_plot}" already exists.' " Choose a different name"
    else:
        artists, legend = plot.setup(
            logevents, plotopts, xaxis_opts, yaxis_opts, args.plot2_type, yaxis2_opts
        )
        handler = EventHandler(artists, legend)
        handler.connect(plt.gcf().canvas)
        plt.show()
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
