# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from collections.abc import Iterable, Sequence
from itertools import islice
from math import exp, log, floor
from random import randint, sample, uniform
from typing import TypeVar

from .interface import SampleAlgorithm


T = TypeVar("T")


def algorithm_R(population: Iterable[T], k: int) -> list[T]:
    """algorithm_R

    A simple and popular but slow algorithm by Alan Waterman.

    http://www.cs.umd.edu/~samir/498/vitter.pdf
    """

    assert k > 0, "Invalid sampling size"
    population = iter(population)
    reservoir = list(islice(population, k))
    if len(reservoir) < k:
        return reservoir

    for i, item in enumerate(population, start=k + 1):
        j = randint(0, i)
        if j < k:
            reservoir[j] = item
    return reservoir


def algorithm_R_factory() -> SampleAlgorithm[T]:
    return algorithm_R


def algorithm_L(population: Iterable[T], k: int) -> list[T]:
    """algorithm_L

    Algorithm L improves upon this algorithm by computing how many items
    are discarded before the next item enters the reservoir. The key
    observation is that this number follows a geometric distribution and
    can therefore be computed in constant time.

    https://dl.acm.org/doi/10.1145/198429.198435
    """

    assert k > 0, "Invalid sampling size"
    population = iter(population)
    reservoir = list(islice(population, k))
    if len(reservoir) < k:
        return reservoir

    W = exp(log(uniform(0, 1)) / k)

    while True:
        skip = floor(log(uniform(0, 1)) / log(1 - W))
        if skip == 0:
            try:
                reservoir[randint(0, k - 1)] = next(population)
            except StopIteration:
                return reservoir
        else:
            skipped_items = tuple(islice(population, skip))
            if len(skipped_items) < skip:
                return reservoir
            reservoir[randint(0, k - 1)] = skipped_items[-1]

        W = W * exp(log(uniform(0, 1)) / k)


def algorithm_L_factory() -> SampleAlgorithm[T]:
    return algorithm_L


def random_sample(population: Iterable[T], k: int) -> list[T]:
    """random_sample

    Basic sample algorithm from the random module. random.sample works
    with sequences, not iterable: it is not a reservoir sampling
    algorithm, so all the log entries must be generated before the
    sampling.
    """

    assert k > 0, "Invalid sampling size"
    if not isinstance(population, Sequence):
        return sample(tuple(population), k)
    else:
        return sample(population, k)


def random_sample_factory() -> SampleAlgorithm[T]:
    return random_sample
