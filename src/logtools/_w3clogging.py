# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from __future__ import annotations
from collections.abc import Callable, Iterable, Iterator
from dataclasses import dataclass, field, fields, make_dataclass
from datetime import datetime, date, time, timedelta, timezone
from functools import partial
from io import SEEK_SET, TextIOWrapper
from itertools import chain, count
import re
from typing import Any, Literal, TextIO, Union, cast, overload

from .interface import AnyEvent, LogEvent, LogParser, SampleFunc


FieldDef = tuple[str, Any, Any]

GMT = timezone(timedelta(0), "GMT")
PREFIXES = ("c", "s", "r", "cs", "sc", "sr", "rs", "x")
_PREFIX = f"(?P<prefix>{'|'.join(PREFIXES)})"
_IDENTIFIER = r"(?P<identifier>.+)"
PREFIX_IDENTIFIER_RE = re.compile(f"{_PREFIX}-{_IDENTIFIER}")
PREFIX_HEADER_RE = re.compile(f"{_PREFIX}\\({_IDENTIFIER}\\)")

HEADER = "HEADER"
PREFIX = "PREFIX"
SIMPLE = "SIMPLE"
# Field types
INTEGER = "Integer"
FIXED = "Fixed Format Float"
URI = "URI"
DATETIME = "Datetime"
DATE = "Date"
TIME = "Time"
STRING = "String"
TEXT = "Text"
NAME = "Name"
ADDRESS = "Address"


@dataclass
class FieldIdentifier:
    type: str
    id: str
    prefix: Union[str, None] = None
    data_type: str = STRING

    @classmethod
    def header(cls, prefix: str, identifier: str) -> FieldIdentifier:
        return cls(HEADER, identifier, prefix, STRING)

    @classmethod
    def with_prefix(
        cls, prefix: str, identifier: str, data_type: Union[str, None] = None
    ) -> FieldIdentifier:
        if data_type is None:
            if identifier == "ip":
                data_type = ADDRESS
            elif identifier == "dns":
                data_type = NAME
            elif identifier == "status":
                data_type = INTEGER
            elif identifier == "comment":
                data_type = TEXT
            elif identifier == "method":
                data_type = NAME
            elif identifier == "uri":
                data_type = URI
            elif identifier == "uri-stem":
                data_type = URI
            elif identifier == "uri-query":
                data_type = URI
            else:
                data_type = STRING
        return cls(PREFIX, identifier, prefix, data_type)

    @classmethod
    def simple(
        cls, identifier: str, data_type: Union[str, None] = None
    ) -> FieldIdentifier:
        if data_type is None:
            if identifier == "date":
                data_type = DATE
            elif identifier == "time":
                data_type = TIME
            elif identifier == "time-taken":
                data_type = FIXED
            elif identifier in ("bytes", "cached", "port"):
                data_type = INTEGER
            else:
                data_type = STRING
        return cls(SIMPLE, identifier, None, data_type)

    def norm_name(self) -> str:
        norm_id = re.sub(r"[^\w]+", "_", self.id)
        if self.type == SIMPLE:
            return norm_id.lower()
        else:
            return f"{self.prefix}_{norm_id}".lower()


def load_field_identifiers(description: Union[str, list[str]]) -> list[FieldIdentifier]:
    fields = []
    if isinstance(description, str):
        identifiers = description.split()
    else:
        identifiers = description
    for identifier in identifiers:
        match_hdr = PREFIX_HEADER_RE.match(identifier)
        match_prefix = PREFIX_IDENTIFIER_RE.match(identifier)
        if match_hdr is not None:
            field = FieldIdentifier.header(*match_hdr.groups())
        elif match_prefix is not None:
            field = FieldIdentifier.with_prefix(*match_prefix.groups())
        else:
            field = FieldIdentifier.simple(identifier)
        fields.append(field)
    return fields


def load_directives(
    fp: TextIOWrapper,
    max_num_lines: Union[int, Literal["*"]] = 4,
    ignore_errors: bool = True,
) -> Iterable[tuple[str, Union[str, datetime, list[FieldIdentifier]]]]:
    if fp.seekable():
        curpos = fp.tell()
    if max_num_lines == "*":
        line_counter: Iterable[int] = count()
    else:
        line_counter = range(max_num_lines)
    for i in line_counter:
        # Idea was to use either `enumerate(fp)` or
        # `zip(range(max_num_lines), fp)` but fp.tell() is disabled when
        # iterating over a file object. I don't know why, apparently for
        # performance reasons. Not a big deal.
        line = fp.readline()
        if not line.startswith("#"):
            if fp.seekable():
                fp.seek(curpos, SEEK_SET)
            break
        try:
            key, value = map(str.strip, line.strip("#").split(":", 1))
        except ValueError:
            if ignore_errors:
                continue
            else:
                if fp.seekable():
                    fp.seek(curpos, SEEK_SET)
                raise
        if key in ("Start-Date", "End-Date", "Date"):
            dt = datetime.strptime(value, "%Y-%m-%d %H:%M:%S").replace(tzinfo=GMT)
            yield key, dt
        elif key == "Fields":
            yield key, load_field_identifiers(value)
        else:
            yield key, value
        if fp.seekable():
            curpos = fp.tell()


MISSING = object()


def make_fn(
    name: str,
    args: list[str],
    body: list[str],
    globals: Union[dict[str, Any], None] = None,
    locals: Union[dict[str, Any], None] = None,
    return_type: Any = MISSING,
) -> Callable[..., Any]:
    if globals is None:
        globals = {}
    if locals is None:
        locals = {}
    return_annotation = ""
    if return_type is not MISSING:
        locals["_return_type"] = return_type
        return_annotation = " -> _return_type"
    _args = ", ".join(args)
    _body = "\n".join(f"    {line}" for line in body)
    txt = f"def {name}({_args}){return_annotation}:\n{_body}"
    exec(txt, globals, locals)
    return cast(Callable[..., Any], locals[name])


def make_field(identifier: FieldIdentifier) -> FieldDef:
    ftype: type = str
    name = identifier.norm_name()
    if identifier.type == SIMPLE:
        if identifier.id == "date":
            ftype = date
        elif identifier.id == "time":
            ftype = time
        elif identifier.id == "port":
            ftype = int
        elif identifier.id == "time-taken":
            name = "duration"
            ftype = int
        elif identifier.id == "bytes":
            name = "size"
            ftype = int
    elif identifier.id == "status" and identifier.prefix == "sc":
        name = "status_code"
        ftype = int
    elif identifier.id == "ip" and identifier.prefix == "s":
        name = "server_host"
    elif identifier.id == "ip" and identifier.prefix == "c":
        name = "remote_host"
    elif identifier.prefix == "cs":
        if identifier.id == "method":
            name = "method"
        elif identifier.id == "uri-stem":
            name = "uri"
        elif identifier.id == "uri-query":
            name = "query"
        elif identifier.id == "username":
            name = "userid"
        elif identifier.id == "user-agent":
            name = "user_agent"
    elif identifier.type == HEADER and identifier.id == "Referer":
        name = "referer"

    return name, Union[ftype, None], field(metadata={"field_identifier": identifier})


def make_str_dundle(field_identifiers: Iterable[FieldIdentifier]) -> Callable[[], str]:
    f_string = []
    for identifier in field_identifiers:
        field_name = identifier.norm_name()
        if identifier.type == SIMPLE and identifier.id == "time-taken":
            field_name = "duration"
        if identifier.type == SIMPLE and identifier.id == "bytes":
            field_name = "size"
        elif identifier.id == "status" and identifier.prefix == "sc":
            field_name = "status_code"
        elif identifier.id == "ip" and identifier.prefix == "s":
            field_name = "server_host"
        elif identifier.id == "ip" and identifier.prefix == "c":
            field_name = "remote_host"
        elif identifier.prefix == "cs" and identifier.id == "method":
            field_name = "method"
        elif identifier.prefix == "cs" and identifier.id == "uri-stem":
            field_name = "uri"
        elif identifier.prefix == "cs" and identifier.id == "uri-query":
            field_name = "query"
        elif identifier.prefix == "cs" and identifier.id == "username":
            field_name = "userid"
        elif identifier.prefix == "cs" and identifier.id == "user-agent":
            field_name = "user_agent"
        elif identifier.type == HEADER and identifier.id == "Referer":
            field_name = "referer"

        if identifier.type == DATE:
            format_spec: Union[str, None] = "%Y-%m-%d"
        elif identifier.type == TIME:
            format_spec = "%H:%M:%S"
        else:
            format_spec = None

        if format_spec is not None:
            f_string.append(
                '{"-" if self.%s is None else format(self.%s, %r)}'
                % (field_name, field_name, format_spec)
            )
        else:
            f_string.append(
                '{"-" if self.%s is None else self.%s}' % (field_name, field_name)
            )
    str_body = [
        "if self._line is not None:",
        "    return self._line",
        "else:",
        "    return f'%s'" % " ".join(f_string),
    ]
    return make_fn("__str__", ["self"], str_body, return_type=str)


def make_w3c_ext_log_event_class(
    field_identifiers: Iterable[FieldIdentifier],
) -> type[LogEvent]:
    fields = [make_field(identifier) for identifier in field_identifiers]
    has_date = any(fid.id == "date" for fid in field_identifiers)
    has_time = any(fid.id == "time" for fid in field_identifiers)
    if has_date and has_time:
        timestamp_field_identifier = FieldIdentifier(
            type=SIMPLE, id="timestamp", prefix=None, data_type=DATETIME
        )
        fields.append(
            (
                "timestamp",
                Union[datetime, None],
                field(metadata={"field_identifier": timestamp_field_identifier}),
            )
        )
    line_field_identifier = FieldIdentifier(
        type=SIMPLE, id="_line", prefix=None, data_type=STRING
    )
    fields.append(
        (
            "_line",
            Union[str, None],
            field(metadata={"field_identifier": line_field_identifier}),
        ),
    )

    def replace(self: AnyEvent, **kwargs: Any) -> AnyEvent:
        for name, value in kwargs.items():
            setattr(self, name, value)
        self._line = None
        return self

    ExtLogEvent = make_dataclass(
        "ExtLogEvent",
        fields,
        bases=(LogEvent,),
        namespace={"replace": replace, "__str__": make_str_dundle(field_identifiers)},
    )
    return ExtLogEvent


def make_w3c_ext_log_event_parser(
    field_identifiers: Iterable[FieldIdentifier],
) -> LogParser:
    ExtLogEvent = make_w3c_ext_log_event_class(field_identifiers)

    class ExtLogParser(LogParser):
        _LogEvent = ExtLogEvent

        def parse_logs(
            self,
            files: Iterable[TextIO],
            tz: timezone = timezone.utc,
            sample: Union[SampleFunc[str], None] = None,
        ) -> Iterator[LogEvent]:
            parse_line = partial(self.parse_line, tz=tz)
            lines = chain.from_iterable(files)
            if sample is not None:
                return map(parse_line, sample(lines))
            else:
                return map(parse_line, lines)

        def serialize_logs(
            self,
            logevents: Iterable[AnyEvent],
            outfile: TextIO,
        ) -> None:
            for event in logevents:
                print(event, end="", file=outfile)

        def has_field(self, name: str) -> bool:
            return name in (field.name for field in fields(ExtLogEvent))  # type: ignore

        # Define the parse_line method just to make abc happy. It will be
        # overridden soon.
        def parse_line(self, line: str, tz: timezone = timezone.utc) -> LogEvent:
            raise NotImplementedError("ExtLogParser.parse_line")

    parse_date_lines = [
        "{_field.name} = _date(",
        "    year=int({date_field}[:4]),",
        "    month=int({date_field}[5:7]),",
        "    day=int({date_field}[8:10]))",
    ]
    parse_time_line = [
        "{_field.name} = _time(",
        "    hour=int({time_field}[:2]),",
        "    minute=int({time_field}[3:5]),",
        "    second=int({time_field}[6:8]),",
        "    tzinfo=tz)",
    ]
    str_body = ["parts = line.split()"]
    create_instance_args = []
    for i, _field in enumerate(fields(ExtLogEvent)):  # type: ignore
        field_identifier = _field.metadata["field_identifier"]
        if field_identifier.data_type == DATE:
            str_body.extend(
                map(
                    lambda line: line.format(_field=_field, date_field=f"parts[{i}]"),
                    parse_date_lines,
                )
            )
            create_instance_args.append(f"    {_field.name}={_field.name},")
        elif field_identifier.data_type == TIME:
            str_body.extend(
                map(
                    lambda line: line.format(_field=_field, time_field=f"parts[{i}]"),
                    parse_time_line,
                )
            )
            create_instance_args.append(f"    {_field.name}={_field.name},")
        elif field_identifier.data_type == DATETIME:
            create_instance_args.append(
                f"    {_field.name}=_datetime.combine(date, time),"
            )
        elif field_identifier.data_type == INTEGER:
            create_instance_args.append(
                f"    {_field.name}=int(parts[{i}]) if parts[{i}] != '-' else None,"
            )
        elif field_identifier.data_type == FIXED:
            create_instance_args.append(
                f"    {_field.name}=float(parts[{i}]) if parts[{i}] != '-' else None,"
            )
        elif field_identifier.id == "_line":
            create_instance_args.append("    _line=line,")
        else:
            create_instance_args.append(
                f"    {_field.name}=parts[{i}] if parts[{i}] != '-' else None,"
            )

    str_body.append("event = ExtLogEvent(")
    str_body.extend(create_instance_args)
    str_body.append(")")
    str_body.append("return event")

    parse_line_fn = make_fn(
        "parse_line",
        ["self", "line", "tz=_timezone.utc"],
        str_body,
        globals={
            "ExtLogEvent": ExtLogEvent,
            "_date": date,
            "_datetime": datetime,
            "_time": time,
        },
        locals={
            "_timezone": timezone,
        },
        return_type=ExtLogEvent,
    )

    setattr(ExtLogParser, "parse_line", parse_line_fn)
    return ExtLogParser()


@overload
def w3c_logging(*, filename: str) -> LogParser: ...


@overload
def w3c_logging(*, fields: str) -> LogParser: ...


def w3c_logging(
    *, filename: Union[str, None] = None, fields: Union[str, None] = None
) -> LogParser:
    """w3clogging

    Log parser that tries to be compatible with the W3C Extended Log
    File Format. The parser uses the directives defined in the header of
    the log file to parse the log entries. One and only one argument
    must be given, either `filename` or `fields`.

    https://www.w3.org/TR/WD-logfile.html

    Arguments:
        filename: file containing the directives on how to parse the
            logs, most likely the same file containing the logs.
        fields: A string describing the fields recorded in the logs. The
            format is the same as the Field directive: a list of field
            separated by a white space.

    Ex:
        w3clogging:filename=/var/log/httpd.log
        "w3clogging:fields=date time s-ip cs-method cs-uri-stem"
    """

    both_params_given = filename is not None and fields is not None
    no_param_given = filename is None and fields is None
    if both_params_given or no_param_given:
        raise TypeError(
            "W3C Ext log file format parser requires one of 'filename' or"
            " 'fields' parameters"
        )
    if filename is not None:
        with open(filename) as fp:
            directives = dict(load_directives(fp))
        # Static checker can not know that "Fields" is always a list of field
        # identifiers.
        field_identifiers = cast(list[FieldIdentifier], directives["Fields"])
    elif fields is not None:
        field_identifiers = load_field_identifiers(fields)
    else:
        raise AssertionError("invalid arguments")

    w3c_ext_log_parser = make_w3c_ext_log_event_parser(field_identifiers)
    return w3c_ext_log_parser
