# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from __future__ import annotations
from collections.abc import Callable, Iterable, Sequence
from dataclasses import astuple, dataclass, fields
from datetime import datetime
from itertools import groupby
from operator import attrgetter
import statistics
import sys
from typing import Union

if sys.version_info >= (3, 9):
    from collections import Counter
else:
    from typing import Counter

from ._filters import is_5xx
from .interface import AnyEvent, LogEvent


Percentile_t = Sequence[Union[int, None]]
Duration = Union[int, float]
FormatCount_t = Callable[[Duration], str]
StatHeader = list[str]
StatRecord = tuple[
    str,
    int,
    float,
    float,
    Union[float, None],
    Union[int, None],
    Union[int, None],
    Percentile_t,
    int,
    float,
    datetime,
    datetime,
    int,
    str,
    int,
    str,
]


@dataclass
class Counters:

    requests: Counter[str]
    requests_time: Counter[str]
    request_errors: Counter[str]


@dataclass
class Stats:

    num_req: int
    mean: float
    median: float
    mode: Union[float, None]
    variance: Union[int, None]
    stdev: Union[int, None]
    percentiles: Percentile_t
    errors: int
    errors_percentage: float
    first_request: datetime
    last_request: datetime
    min_duration: int
    min_duration_page: str
    max_duration: int
    max_duration_page: str
    counters: Counters


def generate_stats(logevents: list[AnyEvent]) -> Stats:
    assert len(logevents) > 0
    num_req = len(logevents)
    first_request = min(logevents, key=attrgetter("timestamp"))
    last_request = max(logevents, key=attrgetter("timestamp"))
    logevents.sort(key=attrgetter("duration"))
    durations = [ev.duration for ev in logevents]
    mean = statistics.mean(durations)
    median = statistics.median(durations)
    mode = statistics.mode(durations)
    try:
        variance: Union[int, None] = statistics.variance(durations, xbar=mean)
    except statistics.StatisticsError:
        variance = None
    try:
        stdev: Union[int, None] = statistics.stdev(durations, xbar=mean)
    except statistics.StatisticsError:
        stdev = None
    try:
        percentiles: Percentile_t = statistics.quantiles(durations, n=100)
    except (AttributeError, statistics.StatisticsError):
        percentiles = [None] * 100
    errors_counter = Counter[str]()
    time_counter = Counter[str]()
    req_counter = Counter[str]()
    for ev in logevents:
        req_counter[ev.uri] += 1
        time_counter[ev.uri] += ev.duration
        if is_5xx(ev):
            errors_counter[ev.uri] += 1
    errors = sum(errors_counter.values())
    errors_percentage = errors / num_req * 100

    stats = Stats(
        num_req=num_req,
        mean=mean,
        median=median,
        mode=mode,
        variance=variance,
        stdev=stdev,
        percentiles=percentiles,
        errors=errors,
        errors_percentage=errors_percentage,
        first_request=first_request.timestamp,
        last_request=last_request.timestamp,
        min_duration=logevents[0].duration,
        min_duration_page=logevents[0].uri,
        max_duration=logevents[-1].duration,
        max_duration_page=logevents[-1].uri,
        counters=Counters(
            requests=req_counter,
            requests_time=time_counter,
            request_errors=errors_counter,
        ),
    )
    return stats


def format_duration(
    value: Union[Duration, None], threshold: float = 2000, scale: float = 1 / 1000
) -> str:
    if value is None:
        return "-"
    if value < threshold:
        if isinstance(value, int):
            return f"{value} ms"
        else:
            return f"{value:.2f} ms"
    return f"{value * scale:.2f} s"


def format_counter(
    counter: Counter[str],
    max_num_entries: int = 5,
    format_count: Union[FormatCount_t, None] = None,
) -> str:
    most_common_entries = counter.most_common(max_num_entries)
    if format_count is not None:
        return "\n".join(
            f" {page:.<70} {format_count(count)}" for page, count in most_common_entries
        )
    else:
        return "\n".join(f" {page:.<70} {count}" for page, count in most_common_entries)


def format_stats(stats: Stats, max_num_entries: int) -> str:
    percentiles = [format_duration(p) for p in stats.percentiles]

    # It would be nice to have the interpolation template strings.
    # https://www.python.org/dev/peps/pep-0501/
    output = f"""
General statistics
 num requests:  {stats.num_req}
 first request: {stats.first_request:%H:%M:%S %d/%m/%Y}
 last request:  {stats.last_request:%H:%M:%S %d/%m/%Y}
 min duration:  {format_duration(stats.min_duration)} ({stats.min_duration_page})
 max duration:  {format_duration(stats.max_duration)} ({stats.max_duration_page})
 mean:          {format_duration(stats.mean)}
 std deviation: {format_duration(stats.stdev)}
 variance:      {format_duration(stats.variance)}
 median:        {format_duration(stats.median)}
 mode:          {format_duration(stats.mode)}
 percentiles:   25th: {percentiles[24]}
                50th: {percentiles[49]}
                75th: {percentiles[74]}
                90th: {percentiles[89]}
                95th: {percentiles[94]}
                99th: {percentiles[98]}
 errors:        {stats.errors} ({stats.errors_percentage:.2f}%)

Most common pages
{format_counter(stats.counters.requests, max_num_entries)}

Most expensive pages (cumulated time)
{format_counter(stats.counters.requests_time, max_num_entries, format_duration)}

Most error pages
{format_counter(stats.counters.request_errors, max_num_entries)}
"""  # noqa: E501
    return output


def generate_stats_text(logevents: list[LogEvent], max_num_entries: int) -> str:
    return format_stats(generate_stats(logevents), max_num_entries)


def generate_stats_csv(
    logevents: list[LogEvent],
    group: Union[str, None] = None,
    include_header: bool = True,
) -> Iterable[Union[StatHeader, StatRecord]]:
    """
    Generate the stats for the given log events in a convient way to
    write them in a CSV file.
    """
    if include_header:
        header = [field.name for field in fields(Stats)]
        if group is not None:
            header.insert(0, group)
        yield header[:-1]
    if group is not None:
        logevents.sort(key=attrgetter(group))
        groups = {
            group: list(events)
            for group, events in groupby(logevents, key=attrgetter(group))
        }
        for group, events in groups.items():
            stats = generate_stats(events)
            yield (group,) + astuple(stats)[:-1]
    else:
        stats = generate_stats(logevents)
        yield astuple(stats)[:-1]
