# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from __future__ import annotations
from abc import ABC, abstractmethod
from collections.abc import Callable, Iterable, Iterator
from datetime import timezone
from typing import Any, TextIO, TypeVar, Union


# Entry points must be factories that return a LogFilter.
LOGFILTER_ENTRY_POINTS_GROUP = "logtools.filters"

# Entry points must be factories that return a LogParser.
LOGPARSER_ENTRY_POINTS_GROUP = "logtools.parsers"

# Entry points must be factories that return a SampleAlgorithm.
SAMPLE_ALGORITHM_ENTRY_POINTS_GROUP = "logtools.sample_algorithms"

T = TypeVar("T")
AnyEvent = Any
LogFilter = Callable[[AnyEvent], bool]
SampleAlgorithm = Callable[[Iterable[T], int], list[T]]
SampleFunc = Callable[[Iterable[T]], list[T]]


class LogEvent:
    @abstractmethod
    def replace(self, **kwargs: Any) -> LogEvent:
        raise NotImplementedError("LogEvent.replace")


class LogParser(ABC):
    @abstractmethod
    def has_field(self, field_name: str) -> bool:
        raise NotImplementedError("LogParser.has_field")

    @abstractmethod
    def parse_logs(
        self,
        files: Iterable[TextIO],
        tz: timezone = timezone.utc,
        sample: Union[SampleFunc[str], None] = None,
    ) -> Iterator[LogEvent]:
        raise NotImplementedError("LogParser.parse_logs")

    @abstractmethod
    def serialize_logs(self, logevents: Iterable[LogEvent], outfile: TextIO) -> None:
        raise NotImplementedError("LogParser.serialize_logs")
