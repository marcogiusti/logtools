# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from __future__ import annotations

from collections.abc import Iterable, Iterator
from dataclasses import dataclass, fields
from datetime import datetime, timezone
from functools import partial
from itertools import chain
from typing import Any, TextIO, Union

from .interface import LogEvent, LogParser, SampleFunc


class W3CExtLogParser(LogParser):
    """w3cextlog

    Custom log parser used to parse real production logs from an IIS
    server in W3C Extended Log File Format. This parser is the first one
    created and it could be deprecated in favor of w3clogging.

    https://www.w3.org/TR/WD-logfile.html

    Arguments:
        None
    """

    def parse_logs(
        self,
        files: Iterable[TextIO],
        tz: timezone = timezone.utc,
        sample: Union[SampleFunc[str], None] = None,
    ) -> Iterator[W3CExtLogEvent]:
        parse_line = partial(self.parse_line, tz=tz)
        lines = chain.from_iterable(files)
        if sample is not None:
            return map(parse_line, sample(lines))
        else:
            return map(parse_line, lines)

    def parse_line(self, line: str, tz: timezone = timezone.utc) -> W3CExtLogEvent:
        parts = line.split()
        if len(parts) != 14:
            raise ValueError(f"invalid line: {line}")
        # this performs 5x better than datetime.strptime
        timestamp = datetime(
            year=int(line[:4]),
            month=int(line[5:7]),
            day=int(line[8:10]),
            hour=int(line[11:13]),
            minute=int(line[14:16]),
            second=int(line[17:19]),
            tzinfo=tz,
        )
        return W3CExtLogEvent(
            remote_host=parts[8],
            userid=parts[7],
            timestamp=timestamp,
            method=parts[3],
            uri=parts[4],
            query=parts[5],
            status_code=int(parts[10]),
            user_agent=parts[9],
            duration=int(parts[13]),
            server_host=parts[2],
            port=int(parts[6]),
            sc_substatus=int(parts[11]),
            sc_win32_status=int(parts[12]),
            _line=line.rstrip("\n"),
        )

    def serialize_logs(  # type: ignore[override]
        self, logevents: Iterable[W3CExtLogEvent], outfile: TextIO
    ) -> None:
        for event in logevents:
            print(event, file=outfile)

    def has_field(self, name: str) -> bool:
        return name in (field.name for field in fields(W3CExtLogEvent))


@dataclass
class W3CExtLogEvent(LogEvent):
    # See:
    # https://www.w3.org/TR/WD-logfile.html
    # https://docs.microsoft.com/en-us/previous-versions/iis/6.0-sdk/ms525807(v=vs.90)
    # https://docs.microsoft.com/en-us/windows/win32/http/w3c-logging

    __slots__ = (
        "remote_host",
        "userid",
        "timestamp",
        "method",
        "uri",
        "query",
        "status_code",
        "user_agent",
        "duration",
        "server_host",
        "port",
        "sc_substatus",
        "sc_win32_status",
        "_line",
    )

    remote_host: str
    userid: str
    timestamp: datetime
    method: str
    uri: str
    query: str
    status_code: int
    user_agent: str
    duration: int
    server_host: str
    port: int
    sc_substatus: int
    sc_win32_status: int
    _line: Union[str, None]

    def replace(self, **kwargs: Any) -> W3CExtLogEvent:
        for name, value in kwargs.items():
            setattr(self, name, value)
        self._line = None
        return self

    def __str__(self) -> str:
        if self._line is not None:
            return self._line
        else:
            return (
                f"{self.timestamp:%Y-%m-%d} {self.timestamp:%H:%M:%S} "
                f"{self.server_host} {self.method} {self.uri} {self.query} "
                f"{self.port} {self.userid} {self.remote_host} "
                f"{self.user_agent} {self.status_code} "
                f"{self.sc_substatus} {self.sc_win32_status} {self.duration}"
            )
