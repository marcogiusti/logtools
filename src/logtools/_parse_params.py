# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE.TwistedMatrixLaboratories for details.

# flake8: noqa

from collections.abc import Iterator


_OP, _STRING = range(2)


def _tokenize(description: str)-> Iterator[tuple[int, str]]:
    """
    Tokenize a strports string and yield each token.

    @param description: a string as described by L{serverFromString} or
        L{clientFromString}.
    @type description: L{str}

    @return: an iterable of 2-tuples of (C{_OP} or C{_STRING}, string).  Tuples
        starting with C{_OP} will contain a second element of either ':' (i.e.
        'next parameter') or '=' (i.e. 'assign parameter value').  For example,
        the string 'hello:greeting=world' would result in a generator yielding
        these values::

            _STRING, 'hello'
            _OP, ':'
            _STRING, 'greeting'
            _OP, '='
            _STRING, 'world'
    """
    empty = ''
    colon = ':'
    equals = '='
    backslash = '\x5c'
    current = empty

    ops = colon + equals
    nextOps = {colon: colon + equals, equals: colon}
    iterdesc = iter(description)
    for n in iterdesc:
        if n in ops:
            yield _STRING, current
            yield _OP, n
            current = empty
            ops = nextOps[n]
        elif n == backslash:
            current += next(iterdesc)
        else:
            current += n
    yield _STRING, current


def parse(description: str) -> tuple[list[str], dict[str, str]]:
    """
    Convert a description string into a list of positional and keyword
    parameters, using logic vaguely like what Python does.

    @param description: a string as described by L{serverFromString} or
        L{clientFromString}.

    @return: a 2-tuple of C{(args, kwargs)}, where 'args' is a list of all
        ':'-separated C{str}s not containing an '=' and 'kwargs' is a map of
        all C{str}s which do contain an '='.  For example, the result of
        C{_parse('a:b:d=1:c')} would be C{(['a', 'b', 'c'], {'d': '1'})}.
    """
    args, kw = [], {}
    colon = ':'
    def add(sofar: tuple[str, ...]) -> None:
        if len(sofar) == 1:
            args.append(sofar[0])
        else:
            kw[sofar[0]] = sofar[1]
    sofar: tuple[str, ...] = ()
    for (type, value) in _tokenize(description):
        if type is _STRING:
            sofar += (value,)
        elif value == colon:
            add(sofar)
            sofar = ()
    add(sofar)
    return args, kw
