# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from collections.abc import Iterator
from importlib import import_module
import sys
from typing import Any, Protocol, Union

from ._parse_params import parse

if sys.version_info >= (3, 10):
    from importlib.metadata import entry_points
else:
    from importlib_metadata import entry_points


class EntryPoint(Protocol):

    def load(self) -> Any: ...


class InvalidSpecError(ImportError):
    pass


def iter_entry_points(
    group: str, name: Union[str, None] = None
) -> Iterator[EntryPoint]:
    if name is not None:
        return iter(entry_points(group=group, name=name))
    else:
        return iter(entry_points(group=group))


def load_entry_point(name: str, group: str) -> Any:
    try:
        ep = next(iter_entry_points(group=group, name=name))
        return ep.load()
    except (ValueError, StopIteration):
        raise ImportError(name)


def import_spec(name: str) -> Any:
    """
    Given a name in the form [package.]module.attr import the
    module and resolve the attributes inside it. The last resolved
    object is returned. Raise ImportError if the the object cannot be
    found (InvalidSpecError if the specifier is invalid).

        resolve('a.b')

    is comparable to:

        a = importlib.import_module('a')
        b = getattr(a, 'b')
    """

    try:
        module_name, attr_name = name.rsplit(".", 1)
    except ValueError:
        raise InvalidSpecError("Invalid name")
    if module_name == "" or attr_name == "":
        raise InvalidSpecError("Invalid name")

    module = import_module(module_name)
    try:
        return getattr(module, attr_name)
    except AttributeError as exc:
        raise ImportError(str(exc))


def resolve(name: str, group: Union[str, None] = None) -> Any:
    if group is not None:
        try:
            obj = load_entry_point(name, group)
        except ImportError:
            obj = import_spec(name)
    else:
        obj = import_spec(name)
    return obj


def resolve_spec(spec: str, group: Union[str, None] = None) -> Any:
    """
    Take a string in the form of <FACTORY_FQN>[:ARGS][:KWARGS] and
    call the factory with the given parameters.

    FACTORY_FQN is the fully qualified name of the callable or the entry
    point name. See below. If the value is a fully qualified name it
    must be in the form of [package.]module.attr.
    ARGS is a list of colon separated arguments. The arguments are
    always treated as strings.
    KWARGS is a list of colon separated keyword arguments in the form of
    name=value. The values are always treated as strings.
    ARGS and KWARGS are passed to the callable as parameters.

    Ex.

        foo.bar:5:spam=spam

    is roughly equivalent to

        import foo

        return foo.bar(5, spam="spam")

    foo is a module or a package, bar is the attribute of the module, 5
    is a *string* parameter and spam=spam is a keyword argument.

    If group parameter is not None, FACTORY_FQN is treated as an entry
    point name of that group.

    The return value is the return value of the factory.
    """

    (name, *args), kwds = parse(spec)
    factory = resolve(name, group)
    obj = factory(*args, **kwds)
    return obj
