# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from datetime import date, datetime
from ipaddress import IPv4Address, IPv4Network, ip_address
from posixpath import splitext
from typing import Union, cast

from .interface import AnyEvent, LogFilter


def is_1xx(logevent: AnyEvent) -> bool:
    """
    Check the status code of the log event, return True if informational
    status (1xx).
    """

    return logevent.status_code < 200  # type: ignore


def is_2xx(logevent: AnyEvent) -> bool:
    """
    Check the status code of the log event, return True if successful
    status (2xx).
    """

    return 200 <= logevent.status_code < 300  # type: ignore


def is_3xx(logevent: AnyEvent) -> bool:
    """
    Check the status code of the log event, return True if redirection
    status (3xx).
    """

    return 300 <= logevent.status_code < 400  # type: ignore


def is_4xx(logevent: AnyEvent) -> bool:
    """
    Check the status code of the log event, return True if client error
    status (4xx).
    """

    return 400 <= logevent.status_code < 500  # type: ignore


def is_5xx(logevent: AnyEvent) -> bool:
    """
    Check the status code of the log event, return True if server error
    status (5xx).
    """

    return logevent.status_code >= 500  # type: ignore


def status_code_eq(value: str) -> LogFilter:
    """
    Convert the string value to a filter function on the status codes of
    the log entries.
    """

    try:
        ivalue = int(value)
    except ValueError:
        if value not in ("1xx", "2xx", "3xx", "4xx", "5xx"):
            raise ValueError("Valid values for status: 1xx, 2xx, 3xx, 4xx, 5xx")
        if value == "1xx":
            return is_1xx
        elif value == "2xx":
            return is_2xx
        elif value == "3xx":
            return is_3xx
        elif value == "4xx":
            return is_4xx
        else:
            return is_5xx
    else:
        if not (100 <= ivalue < 600):
            raise ValueError(f"invalid status code {value}")

        def status_code_eq_filter(logevent: AnyEvent, code: int = ivalue) -> bool:
            return logevent.status_code == code  # type: ignore

        return status_code_eq_filter


def slower_than(value: int) -> LogFilter:
    """
    Return a function that filters out all the log entries slower than the
    given threshold.
    """

    def slower_than_filter(logevent: AnyEvent, value: int = value) -> bool:
        return logevent.duration > value  # type: ignore

    return slower_than_filter


def uri_eq(uri: str) -> LogFilter:
    """
    Return a function that filters out all the log entries with an uri
    that does not match the parameter.
    """

    def uri_filter(logevent: AnyEvent, uri: str = uri) -> bool:
        return logevent.uri == uri  # type: ignore

    return uri_filter


def method_eq(value: str) -> LogFilter:
    """
    Return a function that filters out all the log entries with access
    method that does not match the parameter.
    """

    def method_filter(logevent: AnyEvent, value: str = value) -> bool:
        return logevent.method == value  # type: ignore

    return method_filter


def timestamp_gte(value: Union[datetime, date]) -> LogFilter:
    """
    Return a function that filters out all the log entries before the
    given date and time.
    """

    if isinstance(value, datetime):

        def datetime_gte_filter(
            logevent: AnyEvent, dt: datetime = cast(datetime, value)
        ) -> bool:
            return logevent.timestamp >= dt  # type: ignore

        return datetime_gte_filter

    else:

        def date_gte_filter(logevent: AnyEvent, d: date = cast(date, value)) -> bool:
            return logevent.timestamp.date() >= d  # type: ignore

        return date_gte_filter


def timestamp_lte(value: Union[datetime, date]) -> LogFilter:
    """
    Return a function that filters out all the log entries after the
    given date and time.
    """

    if isinstance(value, datetime):

        def datetime_lte_filter(
            logevent: AnyEvent, dt: datetime = cast(datetime, value)
        ) -> bool:
            return logevent.timestamp <= dt  # type: ignore

        return datetime_lte_filter

    else:

        def date_lte_filter(logevent: AnyEvent, d: date = cast(date, value)) -> bool:
            return logevent.timestamp.date() <= d  # type: ignore

        return date_lte_filter


def remote_host_eq(ip: Union[IPv4Address, IPv4Network]) -> LogFilter:
    """
    If the given parameter is an IP address, the returning function
    filters out all the log entries whose client IP address or server IP
    address is not the same as the given one. If the given parameter is
    an IP network, the returning function filters out all the log
    entries whose client IP address or server IP address are not part of
    the given network.
    """

    if isinstance(ip, IPv4Network):

        def remote_host_in_network_filter(
            logevent: AnyEvent, net: IPv4Network = cast(IPv4Network, ip)
        ) -> bool:
            return (
                logevent.remote_host is not None
                and ip_address(logevent.remote_host) in net
            )

        return remote_host_in_network_filter

    else:
        # use str comparison for faster performance
        def remote_host_eq_filter(logevent: AnyEvent, ip: str = str(ip)) -> bool:
            return logevent.remote_host == ip  # type: ignore

        return remote_host_eq_filter


def file_extension_eq(file_extensions: list[str]) -> LogFilter:
    extensions = set(f".{ext.lower()}" for ext in file_extensions)

    def file_extension_filter(
        logevent: AnyEvent, extensions: set[str] = extensions
    ) -> bool:
        return splitext(logevent.uri)[1].lower() in extensions

    return file_extension_filter


def is_static_file() -> LogFilter:
    return file_extension_eq(
        [
            "css",
            "gif",
            "htm",
            "html",
            "ico",
            "jpg",
            "js",
            "pdf",
            "png",
            "svg",
            "swf",
            "txt",
            "xml",
            "zip",
        ]
    )
