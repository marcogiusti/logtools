# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from dataclasses import dataclass
from datetime import datetime, timedelta, timezone
from ipaddress import IPv4Address
from typing import Iterable, List, TypeVar


T = TypeVar("T")


def dummy_sample(logevents: Iterable[T]) -> List[T]:
    return list(logevents)


CEST = timezone(timedelta(hours=2))


@dataclass
class LogEvent:
    status_code: int = 200
    duration: int = 196
    uri: str = "/apache_pb.gif"
    method: str = "GET"
    timestamp: datetime = datetime(2000, 10, 9, 13, 55, 35, tzinfo=CEST)
    remote_host: IPv4Address = IPv4Address("218.195.205.6")
