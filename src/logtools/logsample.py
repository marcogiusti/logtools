# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from argparse import ArgumentParser
import sys

from ._scripts import (
    Argv,
    ExitVal,
    entry_point,
    make_events_from_args,
    serialize_log_events_from_args,
    stream_common_args_parser,
)


@entry_point
def main(argv: Argv) -> ExitVal:
    parser = ArgumentParser(
        parents=[stream_common_args_parser()],
        description="""
            Read the log events from the given files or the standard input
            and take a sample of K unique events.""",
    )

    args = parser.parse_args(argv)
    with make_events_from_args(args) as logsiter:
        serialize_log_events_from_args(logsiter, args)
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
