# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from bisect import bisect_left
from collections.abc import Iterable
from dataclasses import asdict, astuple, dataclass, fields
from datetime import datetime, timedelta
from operator import attrgetter
from typing import Union

from .interface import LogEvent


QPSStatHeader = list[str]
QPSStatTuple = tuple[datetime, datetime, int, float]
QPSStatRecord = dict[str, Union[datetime, int, float]]


@dataclass
class QPSStat:

    start_time: datetime
    end_time: datetime
    num_samples: int
    qps: float


def qps(logevents: Iterable[LogEvent], window_size: timedelta) -> list[QPSStat]:
    stats: list[QPSStat] = []
    window_size_seconds = window_size.total_seconds()
    timestamps = sorted(map(attrgetter("timestamp"), logevents))
    if len(timestamps) == 0:
        return stats
    lo = 0
    interval_start = timestamps[lo]
    while lo < len(timestamps):
        interval_end = interval_start + window_size
        hi = bisect_left(timestamps, interval_end, lo=lo)
        num_samples = hi - lo
        stat = QPSStat(
            start_time=timestamps[lo],
            end_time=timestamps[hi - 1],
            num_samples=num_samples,
            qps=num_samples / window_size_seconds,
        )
        stats.append(stat)
        lo = hi
        interval_start = interval_end
    return stats


def generate_stats_csv(
    stats: list[QPSStat], include_header: bool = True
) -> Iterable[Union[QPSStatHeader, QPSStatTuple]]:
    if include_header:
        yield [field.name for field in fields(QPSStat)]
    for entry in stats:
        yield astuple(entry)


def generate_stats_json(stats: list[QPSStat]) -> list[QPSStatRecord]:
    return list(map(asdict, stats))
