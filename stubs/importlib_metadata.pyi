# Copyright (c) 2022 Marco Giusti
# See the LICENSE file for more information.

from typing import Any


class EntryPoint:

    def load(self) -> Any: ...

def entry_points(**params: str) -> list[EntryPoint]: ...
