========
Logtools
========

Logtools is a series of tools, inspired by mtools_. They have been used
to help getting more information from a problematic website. The
available tools are:

logfilter
   Read the log events from the given files or the standard input and
   filter out all the lines that do not match the given filters.

logstat
   Generate a report with the statistics of the log events. The
   statistics include duration times with mean and standard deviation,
   most common pages, most pages with errors and most expensive pages in
   cumulated time.

logplot
   Visualize log events in different plot types.

loganon
   Read the log events from the given files or the standard input
   and anonymize the URL.

logsample
   Take a sample of log events.

.. _mtools: https://github.com/rueckstiess/mtools/

Example of usage. `www1.log` is a 106M file, 474392 lines.

``$ time loganon --save-mapping map.json www1.log | logstats``::

   General statistics
    num requests:  474388
    first request: 22:00:00 15/10/2019
    last request:  11:25:31 16/10/2019
    min duration:  0 ms (5rNTnbRJqunWwJ2VDQW18apOe-rUxpW6tufY3UeE6lI)
    max duration:  321.87 s (231M6R1ZQyJoyAJUahFmo94_pj8JGQzZH8SV9kQUU3A)
    mean:          536.47 ms
    std deviation: 4.11 s
    variance:      16912.73 s
    median:        3.00 ms
    mode:          1.00 ms
    percentiles:   25th: 1 ms
                   50th: 3 ms
                   75th: 43 ms
                   90th: 214 ms
                   95th: 555 ms
                   99th: 19.31 s
    errors:        2231 (0.47%)

   Most common pages
    RMiYYxXXAwx7hiN-fO68S_gEeSkUNjrHoeILtIhCvP0........................... 27271
    GrPA1DMH4-eLn4k34toPTfKiEM4dIk_E48PqyZXGQjI........................... 17455
    NPjmuv19sLy_zZmCKRWBSCghMxVNvsml_drZxiKUBWo........................... 15049
    QKG0AW1AcyhzCLtYpKOMqSY0ep0wUeuTArHW71UwfLA........................... 12285
    uMlr9Fs68pXOUEbvaY2W-pqd9XZyu5WgTlzaDcOOZZs........................... 11951

   Most expensive pages (cumulated time)
    6dJSpzPNViOUQrlAvCRC18TOXrAydi7ePbQvesdOXx8........................... 43280519
    Gtb1gzmI8qUub44KFGLiDKRg9_AXtwbRbHi4ZovngWg........................... 23827146
    NPjmuv19sLy_zZmCKRWBSCghMxVNvsml_drZxiKUBWo........................... 22461307
    RMiYYxXXAwx7hiN-fO68S_gEeSkUNjrHoeILtIhCvP0........................... 19640610
    uMlr9Fs68pXOUEbvaY2W-pqd9XZyu5WgTlzaDcOOZZs........................... 16813168

   Most error pages
    6dJSpzPNViOUQrlAvCRC18TOXrAydi7ePbQvesdOXx8........................... 351
    RMiYYxXXAwx7hiN-fO68S_gEeSkUNjrHoeILtIhCvP0........................... 317
    uMlr9Fs68pXOUEbvaY2W-pqd9XZyu5WgTlzaDcOOZZs........................... 303
    NPjmuv19sLy_zZmCKRWBSCghMxVNvsml_drZxiKUBWo........................... 294
    Gtb1gzmI8qUub44KFGLiDKRg9_AXtwbRbHi4ZovngWg........................... 264

   real    0m11.837s
   user    0m16.555s
   sys     0m0.560s

For more examples, see the `docs <docs/>`_ folder.

.. vim: ft=rst tw=72
